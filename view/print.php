<!doctype html>
<html ng-app="punchco">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Grubgrabr</title>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/css/bootstrap-flatly.css" />
    <link rel="stylesheet" type="text/css" href="/css/jquery.qtip.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/main.css" />
  </head>
  <?php flush(); ?>
  <body>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-44927842-1', 'grubgrabr.com');
      ga('send', 'pageview');
    </script>
    <?php include('templates/print.html') ?>
    <script language="javascript" type="text/javascript">var bomb = <?php echo json_encode($MODEL); ?></script>
    <script src="/js/jquery-2.0.3.min.js"></script>
    <script src="/js/underscore-min.js"></script>
    <script src="/js/angular.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/moment.min.js"></script>
    <script src="/js/index.js"></script>
    <script src="/js/qrcode.min.js"></script>
    <script src="/js/jquery.qtip.min.js"></script>
  </body>
</html>
