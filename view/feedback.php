<!doctype html>
<html ng-app="punchco">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Grubgrabr</title>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/css/bootstrap-flatly.css" />
    <link rel="stylesheet" type="text/css" href="/css/jquery.qtip.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/main.css" />
  </head>
  <?php flush(); ?>
  <body>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-44927842-1', 'grubgrabr.com');
      ga('send', 'pageview');
    </script>
    

<div class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand" href="http://grubgrabr.com/">Grubgrabr</a>
    </div>
  </div>
</div>
<div class="container">
  <div ng-controller="ViewFeedbackCtrl">
    <div class="row" style="margin-top:60px;">
      <h3 ng-bind="code.businessName"></h3>
      <p><span>Reviewer: </span><strong ng-bind="code.reviewer"></strong></p>
      <p>Rating: <strong ng-bind="code.rating + ' Stars'"></strong></p>
      <p ng-show="code.comment.length">Comment: <strong ng-bind="code.comment"></strong></p>
    </div>
    <div class="row">
      <img ng-src="{{cfurl}}/{{code.imageKey}}" />
    </div>
    <div class="row">
      <div class="col-md-12">
        <ul id="printlist">
          <li ng-repeat="code in codes">
            <center>
              <h3 ng-bind="code.code + '.ggco.de'"></h3>
              <div id="{{code.id}}"></div>
            </center>
          </li>
        </ul>
      </div>
    </div>
  </div>

</div>
<div class="container" style="margin-top:25px;">
  <div class="page-header"></div>
  <div class="row text-center">
    <span class="help-block">&copy; 2013 Grubgrabr, v. 0.1 (beta)</span>
  </div>
  <div class="row text-center">
    <span class="help-block" xcontact style="margin-left:20px;cursor:pointer;">Contact Us</span>
  </div>
</div>




    <script language="javascript" type="text/javascript">var bomb = <?php echo json_encode($MODEL); ?></script>
    <script src="/js/jquery-2.0.3.min.js"></script>
    <script src="/js/underscore-min.js"></script>
    <script src="/js/angular.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/moment.min.js"></script>
    <script src="/js/index.js"></script>
    <script src="/js/qrcode.min.js"></script>
    <script src="/js/jquery.qtip.min.js"></script>
  </body>
</html>
