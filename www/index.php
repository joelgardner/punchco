<?php
date_default_timezone_set('America/Chicago');

	// TODO: wire includes better
	require '../lib/Stripe.php';
	require '../lib/Slim/Slim.php';
	require '../lib/aws/aws-autoloader.php';
	require '../lib/PHPMailer/PHPMailerAutoload.php';
	require '../service/Logger.php';
	require '../service/SessionService.php';
	require '../service/ValidationService.php';
	require '../service/DataAccess.php';
	require '../service/StripeService.php';
	require '../service/AccountService.php';
	require '../service/CodeService.php';
	require '../service/RewardService.php';
	require '../service/FeedbackService.php';
	require '../service/ImageService.php';
	require '../service/EmailService.php';
	require '../controller/BaseController.php';
	require '../controller/DefaultController.php';
	require '../controller/JoinController.php';
	require '../controller/CodesController.php';
	require '../controller/FeedbackController.php';
	require '../controller/RewardsController.php';
	require '../controller/AccountController.php';
	require '../controller/LoginController.php';
	require '../controller/ApiController.php';
	require '../controller/ProductController.php';
	require '../controller/PrintController.php';
	require '../config/'.$_SERVER['LIVENESS'].'/app.config';
	require '../config/'.$_SERVER['LIVENESS'].'/db.config';

	// load Slim and initialize the session
	\Slim\Slim::registerAutoloader();
	session_start();
	$app = new \Slim\Slim(array('log.writer' => new Logger()));
	$app->add(new \Slim\Middleware\ContentTypes());
	
	// logging
	$logger = $app->getLog();
	$logger->setLevel(\Slim\Log::DEBUG);

	// parse the subdomain from the url, if any
	global $subdomain;
	$parts = explode(".", $_SERVER["SERVER_NAME"]);
	if (count($parts) == 3) {	// example: abcd123.grubgrabr.com --> ["abcd123", "grubgrabr", "com"]
		$subdomain = $parts[0];
		(new Logger())->write('Parsed subdomain '.$subdomain);

		// redirect abcd123.grubgrabr.com to grubgrabr.com/?ggcode=abcd123 (only add querystring if abcd123 is a valid code)
		$redeemResult = (new CodeService())->punchcode_redeem(array('code' => $subdomain));
		redirect_user("http://grubgrabr.com/".($redeemResult["Result"] != 1 ? "?ggcode=".$subdomain : ""));
	}

	// routing
	$app->map('/upload/image/:code', 'imageUploader')->via('POST');
	$app->get('/!:code', 'showFeedback');
	$app->get('/tmpl/:viewName', 'renderTemplate');
	$app->get('/tmpl/:sectionName/:viewName', 'renderTemplate2');
	$app->get('/tmpl/:sectionName/:viewName', 'renderTemplate2');
	$app->map('/api/:service/:action', 'apiController')->via('GET', 'POST');
	$app->get('/', 'controllerAction');
	$app->map('/:controller', 'controllerAction')->via('GET', 'POST');
	$app->map('/:controller/:action', 'controllerAction')->via('GET', 'POST');
	$app->map('/:controller/:action/:id', 'controllerAction')->via('GET', 'POST');
	$app->run();

	/// routing handlers
	function imageUploader($code) {
		if ($_FILES['fileName']['error']) {
			$logger = new Logger();
			$logger->write('error code for failed upload: '.$_FILES['fileName']['error']);
			exit;
		}

		// make sure this code is ratings&reviews enabled
 		$result = (new CodeService())->get_code($code);
 		if (isset($result['rr']) && $result['rr'] == 1) {
 			$imageService = new ImageService();
			$imageService->SaveUploadedImage($_FILES['fileName']['tmp_name'], $_FILES['fileName']['name'], $code);
 		}
	}

	function showFeedback($code) {
		controllerAction('feedback', 'DisplayFeedback', $code);
	}

	function controllerAction($controller = null, $action = null, $id = null) {
		$logger = new Logger();
		//$logger->write('controller ['.$controller.'] action: ['.$action.'] id: ['.$id.']');
		$controllerClassName = ucfirst($controller ? $controller : "Default")."Controller";
		if ($controller && !ctype_lower($controller) && class_exists($controllerClassName)) {
			// it is a valid controller name, however there is at least 1 capital letter in the controller name which messes up AngularJS's routing,
			// so we redirect to a strtolower'ed version of hte controller's name.  such is life.
			$logger->write('Redirecting to lowercase version');
			redirect_user("/".strtolower($controller));
		}
		else if (!class_exists($controllerClassName)) {
			// invalid controller name, redirect to the homepage
			$logger->write('Invalid controller name: '.$controllerClassName);
			redirect_user("/");
		}

		$controllerAction = array(new $controllerClassName(array(\Slim\Slim::getInstance()->request())), $action ? $action : "Index");
		call_user_func_array($controllerAction, [$id]);
	}

	function apiController($service, $action) {
		$api = new ApiController(\Slim\Slim::getInstance()->request());
		$params = \Slim\Slim::getInstance()->request()->getBody();
		$api->Execute($service, $action, $params ? $params : []);
	}

	// TODO: refactor these handlers
	function renderTemplate($viewName) {
		global $subdomain;
		if ($subdomain) {
			include("../view/templates/brand/$viewName");
		}
		else {
			include("../view/templates/$viewName");
		}
	}
	function renderTemplate2($sectionName, $viewName) {
		global $subdomain;
		if ($subdomain) {
			include("../view/templates/brand/$viewName");
		}
		else {
			include("../view/templates/$sectionName/$viewName");
		}
	}

	// helpers
	function redirect_user($url) {
		header("Status: 200");
    	header("Location: ".$url);
		exit;
	}
?>