angular.module('punchco.service', [], function($provide) {
    $provide.factory('api', ['$http', function($http) {
        return {
            account: {
                login: function(options) {
                    return $http.post("/api/account/login", { options: options });
                },
                logout: function() {
                    return $http.get('/api/account/logout');
                },
                create: function(options) {
                    return $http.post('/api/account/create', { options:options });
                },
                get: function(userId) {
                    return $http.get('/api/user/' + userId);
                },
                getCurrent: function() {
                    return $http.get('/api/account/current');
                },
                submitPayment: function(options) {
                    return $http.post('/api/account/submitpayment', { options: options });
                },
                addCard: function(options) {
                    return $http.post('/api/account/addcard', { options: options });
                },
                delete_paymentinfo: function(options) {
                    return $http.post('/api/account/delete_paymentinfo', { options: options });
                },
                get_payment_info: function() {
                    return $http.get('/api/account/get_payment_info');
                }
            },
            code: {
                create: function(options) {
                    return $http.post('/api/code/create', { options: options })
                },
                batch: function(options) {
                    return $http.post('/api/code/batch', { options: options })
                },
                search: function(options) {
                    return $http.post('/api/code/search', { options: options })
                },
                getBatches: function() {
                    return $http.post("/api/code/getbatches");
                },
                redeem: function(options) {
                    return $http.post('/api/code/redeem', { options: options });
                },
                punchcode_redeem: function(options) {
                    return $http.post('/api/code/punchcode_redeem', { options: options });
                },
                recentactivity: function(options) {
                    return $http.post('/api/code/recentactivity', { options: options });
                },
                get_code_stats: function() {
                    return $http.get('/api/code/get_code_stats');
                },
                submitRating: function(options){
                    return $http.post('/api/code/submitrating', { options: options });
                }
            },
            reward: {
                create: function(options) {
                    return $http.post('/api/reward/create', { options: options });
                },
                get: function() {
                    return $http.post('/api/reward/get');
                },
                userrewards: function(options) {
                    return $http.post('/api/reward/userrewards', { options: options });
                },
                redeem: function(options) {
                    return $http.post('/api/reward/redeem', { options: options });
                },
                myrewards: function(options) {
                    return $http.post('/api/reward/myrewards', { options: options });
                }
            },
            feedback: {
                get_feedback: function() {
                    return $http.get('/api/feedback/get');
                }
            }
        }
    }]);
    $provide.factory('tableService', [function() {
        return {
            refreshPageNumberArray: function(_resultsPerPage) {
                this.resultsperpage = _resultsPerPage;
                this.pagenumbers = [];
                var i = 1;
                for (i = 1;i < (this.items.length / this.resultsperpage) + 1;++i) {
                    this.pagenumbers.push(i);
                }
                if (--i < this.pageNum) {
                    this.pageNum = i;
                }
                if (this.pageNum==0) this.pageNum=1; // TODO: the fact this line is required is a bug, fix
                this.refresh();
            },
            page: function(_pageNum) {
                this.pageNum = _pageNum;
                this.refresh();
            },
            sort: function(column) {
                if (this.sortCol == column) {
                    this.sortAsc = !this.sortAsc;
                }
                this.sortCol = column;
                var _itemsAsc = _.sortBy(this.items, function (item) { return item[this.sortCol];  }, this);
                this.items = this.sortAsc ? _itemsAsc : _itemsAsc.reverse();
                this.refresh();
            },
            refresh: function() {
                var results = [];
                var start = (this.pageNum - 1) * this.resultsperpage;
                for(i = start;i < this.pageNum * this.resultsperpage && i < this.items.length;++i) {
                    results.push(this.items[i]);
                }
                this.displayedResults = results;
            }
         }
    }]);
    $provide.factory('stripe', [function() {
        return {
            tokenize: function(cardNumber, cvc, expireMonth, expireYear, handler) {
                if (bomb.stripePublicKey) {
                    Stripe.setPublishableKey(bomb.stripePublicKey);
                }
                Stripe.card.createToken({
                    number: cardNumber,
                    cvc: cvc,
                    exp_month: expireMonth,
                    exp_year: expireYear
                }, handler);
            },
            validateCardNumber: function(cardNumber) {
                return typeof Stripe !== 'undefined' && Stripe.card.validateCardNumber(cardNumber);
            },
            validateExpiry: function (mo, yr) {
                return typeof Stripe !== 'undefined' && Stripe.card.validateExpiry(mo, yr);
            },
            validateCVC: function(cvc) {
                return typeof Stripe !== 'undefined' && Stripe.card.validateCVC(cvc);
            },
            cardType: function(cardNumber) {
                return typeof Stripe !== 'undefined' && Stripe.card.cardType(cardNumber);
            }
        }
    }]);
});

var url = window.location.host.split(".");
angular.module('punchco.directive', [])
    .directive('sortIcon', function () {
        return {
            restrict: 'A',
            template: '<i style="float:right" class="glyphicon" ng-show="table.sortCol==columnKey" ng-class="{\'glyphicon-chevron-up\':table.sortAsc, \'glyphicon-chevron-down\':!table.sortAsc}"></i>',
            scope: {
                table: '=',
                columnKey: '='
            },
            link: function (scope, element, attrs) {
                //console.log('in sortIcon.link');
            }
        
        }
    })
    .directive('pager', function () {
        return {
            restrict: 'A',
            template:       '<div>' +
                                '<div>' +
                                    '<span ng-bind="table.items.length + \' results\'"></span>' +
                                '</div>' +
                                '<div class="btn-toolbar">' + 
                                  '<div class="btn-group">' +
                                    '<button ng-repeat="num in table.pagenumbers" type="button" class="btn btn-success btn-sm" ng-class="{active:table.pageNum==num}" ng-click="table.page(num)" ng-bind="num"></button>' +
                                  '</div>' +
                                  '<div class="btn-group">' +
                                    '<button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown">' +
                                      '<span ng-bind="table.resultsperpage + \' Per Page \'"></span>' +
                                      '<span class="caret"></span>' +
                                    '</button>' +
                                    '<ul class="dropdown-menu">' +
                                      '<li ng-repeat="rpp in [3, 5, 10, 25, 50]"><a href="#" ng-click="table.refreshPageNumberArray(rpp);" ng-bind="rpp"></a></li>' +
                                    '</ul>' +
                                  '</div>' + 
                                '</div>' +
                            '</div>',
            scope: {
                table: '='
            },
            link: function (scope, element, attrs) {
                //console.log('in pager.link');
            }
        
        }
    })
    .directive('xqtip', function($http, $compile, $templateCache){
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var compiledContent = $compile(
                        '<h4>Permanent</h4>' +
                        '<span>Ratings & Reviews: <em ng-bind="code_stats.num_permanent_rr.toLocaleString()"></em></span>' +
                        '<span>Regular: <em ng-bind="code_stats.num_permanent.toLocaleString()"></em></span>' +
                        '<h4>Subscription</h4>' +
                        '<span>Ratings & Reviews: <em ng-bind="code_stats.num_subscription_rr.toLocaleString()"></em></span>' +
                        '<span>Regular: <em ng-bind="code_stats.num_subscription.toLocaleString()"></em></span>'
                )(scope);
                $(element).qtip({
                    content: {
                        text: compiledContent,
                        title: {
                            text: '<h5 style="padding-right:20px;">Available Grubcode Breakdown</h5>',
                            button: true
                        }
                    },
                    show: { event: 'click' },
                    position: {
                      my: 'top center',
                      at: 'bottom center',
                      target: $(element),
                      viewport: $(window)
                    },
                    hide: {
                      fixed : true,
                    },
                    style: {
                        classes: 'qtip-bootstrap'
                    }
                });
            }
        }
    })
    .directive('xcontact', function($http, $compile, $templateCache){
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var compiledContent = $compile(
                        '<h5>Send us an Email!</h5>' +
                        '<span class="help-block">joel@grubgrabr.com</span>'
                )(scope);
                $(element).qtip({
                    content: {
                        text: compiledContent,
                        title: {
                            text: '<h4 style="padding-right:20px;">Contact Grubgrabr</h4>',
                            button: true
                        }
                    },
                    show: { event: 'click' },
                    position: {
                      my: 'bottom center',
                      at: 'top center',
                      target: $(element),
                      viewport: $(window)
                    },
                    hide: {
                      fixed : true,
                    },
                    style: {
                        classes: 'qtip-bootstrap'
                    }
                });
            }
        }
    });

angular.module('punchco.filter', [])
    .filter('codeStatus', function () {
        return function(codes, showUnclaimed, showClaimed, showRedeemed) {
            if (!codes) {
                return false;
            }
            return _.filter(codes, function (c) { 
                return (c.statusId==1&&showUnclaimed) ||
                    (c.statusId==2&&showClaimed) ||
                    (c.statusId==4&&showRedeemed)
            });
        }
    })
    .filter('dateFilter', function() {
        return function(date) {
            return moment(date, 'YYYY-MM-DD HH:mm:ss').format('MMM D, YYYY h:mma')
        }
    })
    .filter('userRewardFilter', function () {
        return function(rewards, showAll) {
            if (!rewards) {
                return false;
            }
            if (showAll) {
                return rewards;
            }
            
            return rewards.slice(0, 10);
        }
    });

angular.module('punchco', ['punchco.service', 'punchco.directive', 'punchco.filter'], function ($locationProvider, $routeProvider) {
    $locationProvider.html5Mode(true);
    $routeProvider
        .when('/', {
            templateUrl: 'tmpl/default.html', 
            controller: DefaultCtrl,
            resolve: {
                user: [function () {
                    return bomb.user ? bomb.user : null
                }],
                account: ['api', function (api) {
                    return {
                        name: (bomb.account ? bomb.account.name : '')
                    }
                }],
                mycodes: [function() { 
                    return bomb.mycodes ? bomb.mycodes : null 
                }], 
                myrewards: [function() {
                    return bomb.myrewards ? bomb.myrewards : null
                }],
                userrewards: [function() { 
                    return bomb.userrewards ? bomb.userrewards : null 
                }]
            }
        })
        .when('/codes', {
            templateUrl: '/tmpl/codes.html', 
            controller: CodesCtrl,
            resolve: {
                user: ['api', function (api) {
                    return bomb.user;
                }],
                batches: ['api', function (api) {
                    if (!bomb || !bomb.batches) {
                        return api.code.getBatches();
                    }
                    return bomb.batches;
                }],
                paymentinfo: ['api', function (api) { 
                    if (!bomb || !bomb.paymentinfo) {
                        return api.account.get_payment_info();
                    }
                    return bomb.paymentinfo 
                }],
                code_stats: ['api', function(api) { 
                    if (!bomb || !bomb.code_stats) {
                        return api.code.get_code_stats();
                    }
                    return bomb.code_stats 
                }]
            }
        })
        .when('/join', {
            templateUrl: 'tmpl/join.html', 
            controller: SignupCtrl
        })
        .when('/rewards', {
            templateUrl: 'tmpl/rewards.html', 
            controller: RewardsCtrl,
            resolve: {
                rewards: ['api', function (api) {
                    if (!bomb || !bomb.rewards) {
                        return api.reward.get();
                    }
                    return bomb.rewards;
                }]
            }
        })
        .when('/account', {
            templateUrl: 'tmpl/account.html', 
            controller: AccountCtrl,
            resolve: {
                user: ['api', function (api) {
                    if (!bomb || !bomb.user) {
                        return api.account.getCurrent();
                    }
                    return bomb.user;
                }],
                paymentinfo: ['api', function(api) {
                    if (!bomb || !bomb.paymentinfo) {
                        return api.account.get_payment_info();
                    }
                    return bomb.paymentinfo 
                }]
            }
        })
        .when('/product', {
            templateUrl: 'tmpl/product.html', 
            controller: ProductCtrl,
            resolve: {
                
            }
        })
        .when('/feedback', {
            templateUrl: 'tmpl/feedback.html', 
            controller: FeedbackCtrl,
            resolve: {
                user: ['api', function (api) {
                    if (!bomb || !bomb.user) {
                        return api.account.getCurrent();
                    }
                    return bomb.user;
                }],
                feedback: ['api', function(api) {
                    if (!bomb || !bomb.feedback) {
                        return api.feedback.get_feedback();
                    }
                    return bomb.feedback 
                }]
            }
        });
});

function NavBarCtrl ($rootScope, $scope, $location, api) {
    $scope.user = bomb.user ? bomb.user : null;
    $scope.path = $location.path();
    $scope.showLoginInputs = false;

    $scope.logout = function() {
        api.account.logout().then(function(response) {
            $scope.user.id = 0;
            $scope.loginUsername = null;
            $scope.loginPassword = null;
            bomb.user = null;
            $location.path('/');
            $rootScope.$broadcast('loggedOut');
        });
    }
    $scope.code = {};
    $scope.code.default = function() { $scope.path = '/codes'; $location.path($scope.path) }
    $scope.code.create = function() { $scope.path = '/codes/create'; $location.path($scope.path) }

    $scope.home =    function() { $scope.path = '/'; $location.path($scope.path); }
    $scope.codes =      function() { $scope.path = '/codes'; $location.path($scope.path); }
    $scope.rewards =    function() { $scope.path = '/rewards'; $location.path($scope.path); }
    $scope.feedback =   function() { $scope.path = '/feedback'; $location.path($scope.path); }
    $scope.account =    function() { $scope.path = '/account'; $location.path($scope.path); }
    $scope.signup =     function() { $location.path('/join'); }
    $scope.loginError = null;
    $scope.login = function() { 
        if (!$scope.showLoginInputs) {
            $scope.showLoginInputs = true;
            return;
        }

        $scope.loginError = null;
        api.account.login({
            username: $scope.loginUsername, 
            password: $scope.loginPassword
        }).then(function(response) {
            if (!response.data.user || !response.data.user.id) {
                $scope.loginError = response.data.errorDesc;
                $scope.loginPassword = null;
                return;
            }

            response.data.cfurl = bomb.cfurl;
            bomb = response.data;
            var path = $location.path();
            if (!response.data.user.businessId && path != '/') {
                $scope.path = '/'; 
            }
            else if (response.data.user.businessId && path != '/codes') {
                $scope.path = '/codes';
            }
            $location.path($scope.path);
            $scope.user = response.data.user;
            $rootScope.$broadcast('loggedIn', response.data);
        });
    }

    $scope.$on('loggedIn', function(event, data) {
        $scope.user = data.user;
        bomb.user = $scope.user;
    });
    $scope.$on('viewChanged', function(event, data) {
        $scope.path = data;
    });
}

function DefaultRewardsCtrl($scope, $location, $filter, api, tableService) { 
    $scope.selectedRewardId = 0;

    $scope.showClaimed = true;
    $scope.showRedeemed = false;

    $scope.rewardsTable = {
        pageNum: 1,
        resultsperpage: 5,
        sortAsc: false,
        sortCol: 'punched'
    };
    $scope.rewardsTable = _.extend($scope.rewardsTable, tableService);
    
    $scope.rowSelected = function(reward) {
        $scope.selectedRewardId = reward.rewardCodeId;
    }

    $scope.$on('loggedIn', function(event, data) {
        $scope.myrewards = data.myrewards;
        $scope.rewardsTable.items = $filter('codeStatus')($scope.myrewards, false, $scope.showClaimed, $scope.showRedeemed);
        $scope.rewardsTable.refreshPageNumberArray(5);
        bomb.myrewards = data.myrewards;
    });

    $scope.$on('rewardRedeemed', function(event, data) {
        var reward = data.reward;
        //var affectedCodes = data.punchedCodes;

        $scope.myrewards.unshift(reward);
        $scope.rewardsTable.items = $scope.myrewards;
        $scope.rewardsTable.refreshPageNumberArray(5);
        bomb.myrewards = $scope.myrewards;

        //alert('in DefaultRewardsCtrl: \nreward:\n' + JSON.stringify(reward));
    });

    $scope.toggle = function(status) {
        switch (status) {
            case 2: $scope.showClaimed = !$scope.showClaimed; break;
            case 4: $scope.showRedeemed = !$scope.showRedeemed; break;
        }
        $scope.rewardsTable.items = $filter('codeStatus')($scope.myrewards, false, $scope.showClaimed, $scope.showRedeemed);
        $scope.rewardsTable.refreshPageNumberArray(5);
    }

    $scope.rewardsTable.items = $filter('codeStatus')($scope.myrewards, false, $scope.showClaimed, $scope.showRedeemed);
    $scope.rewardsTable.refreshPageNumberArray(5);
}

function DefaultCodesCtrl($rootScope, $scope, $location, $filter, api, tableService) {
    $scope.mycodes = bomb.mycodes;
    $scope.showClaimed = true;
    $scope.showRedeemed = true;

    $scope.activityTable = {
        pageNum: 1,
        resultsperpage: 5,
        sortAsc: false,
        sortCol: 'punched'
    };
    $scope.activityTable = _.extend($scope.activityTable, tableService);

    $scope.codeSelected = function(code) {
        $scope.selectedCodeId = code.codeId;
    }

    $scope.$on('loggedIn', function(event, data) {
        $scope.mycodes = data.mycodes;
        $scope.activityTable.items = $scope.mycodes;
        $scope.activityTable.refreshPageNumberArray(5);
        bomb.mycodes = data.mycodes;
    });

    $scope.$on('codeRedeemed', function(event, code) {
        $scope.mycodes.unshift(code);
        $scope.activityTable.items = $scope.mycodes;
        $scope.activityTable.refreshPageNumberArray(5);
        bomb.mycodes = $scope.mycodes;
    });

    $scope.$on('rewardRedeemed', function(event, data) {
        var affectedCodes = data.punchedCodes;
        _.each(affectedCodes, function (code, index) {
            var c = _.findWhere($scope.mycodes, { codeId: code.id });
            c.num_punches = code.num_punches;
            c.statusId = code.statusId;
            
        })
    });

    $scope.activityTable.items = $filter('codeStatus')($scope.mycodes, false, $scope.showClaimed, $scope.showRedeemed);
    $scope.activityTable.refreshPageNumberArray(5);

    $scope.toggle = function(status) {
        switch (status) {
            case 2: $scope.showClaimed = !$scope.showClaimed; break;
            case 4: $scope.showRedeemed = !$scope.showRedeemed; break;
        }
        $scope.activityTable.items = $filter('codeStatus')($scope.mycodes, false, $scope.showClaimed, $scope.showRedeemed);
        $scope.activityTable.refreshPageNumberArray(5);
    }

    $scope.showRR = function(code) {
        $rootScope.$broadcast('showFeedback', code);
    }

}

function DefaultRedeemCtrl($rootScope, $scope, $location, api) {
    $scope.redeem = function() {
        api.reward.redeem({
            rewardId: $scope.activeReward.rewardId
        }).then(function(response) {
            if (!response.data.reward) {
                var errorText = response.data.Feedback;
                alert(errorText);
                return;
            }
            var reward = response.data.reward;
            var userreward = _.findWhere($scope.userrewards, { id: parseInt(reward.businessId) });
            userreward.availPunches = parseInt(userreward.availPunches) - parseInt(reward.required_punches);
            $rootScope.$broadcast('rewardRedeemed', response.data);
            //$scope.activeReward = null;
            $scope.successMsg = "You've successfully redeemed reward " + response.data.reward.rewardName;
        });
    };

    $scope.rowSelected = function (reward) {
        $scope.activeReward = reward;
        $scope.successMsg = null;
    };

    $scope.$on('loggedIn', function(event, data) {
        $scope.userrewards = data.userrewards;
        bomb.userrewards = data.userrewards;
    });

    $scope.$on('codeRedeemed', function(event, code) {
        if (!$scope.userrewards || !$scope.userrewards.length) {
            return;
        }
        var userreward = _.findWhere($scope.userrewards, { id: parseInt(code.businessId) });
        userreward.availPunches = parseInt(userreward.availPunches) + parseInt(code.num_punches);
    });
}

function DefaultCtrl ($rootScope, $scope, $location, api, user, mycodes, myrewards, userrewards) {
    $scope.user = user;
    $scope.yellowDialog = false;
    $scope.punchCode = null;
    $scope.mycodes = mycodes;
    $scope.myrewards = myrewards;
    $scope.userrewards = userrewards;

    $scope.tabs = {
        active: 'My Codes',
        nodes: [
            { title: 'My Codes' },
            { title: 'My Rewards'},
            { title: 'Redeem' }
        ],
        action: function (node) {
            this.active = node.title;
        }
    };

    $scope.redeem = function() {
        api.code.punchcode_redeem({
            code:$scope.punchCode
        }).then(function(response) {
            switch(response.data.Result) {
                case '0':
                    $rootScope.$broadcast('codeRedeemed', response.data);
                    $scope.user.score = parseInt($scope.user.score) + parseInt(response.data.num_punches);
                    if (params.ggcode) {
                        $location.url($location.path());
                    }
                    if (response.data.rr == 1) {
                        $rootScope.$broadcast('showFeedback', response.data);
                    }
                case '1':
                    $scope.punchCode = null;
                    if (params.ggcode) {
                        $location.url($location.path());
                    }
                    break;
                case '2':
                    $scope.yellowDialog = true;
                    if (response.data.rr == 1) {
                        $rootScope.$broadcast('showFeedback', response.data);
                    }
                    break;
                default:
                    $scope.punchCode = null;
                    if (params.ggcode) {
                        $location.url($location.path());
                    }
                    break;
            }
            
        });
    }

    $scope.$on('ratingSubmitted', function(event, data) {
        //alert(JSON.stringify(data));
        if (data.Result == 0 && $scope.user) {
            $scope.user.score += 1;
        }
    });

    var params = $location.search();
    if (params.ggcode) {
        $scope.punchCode = params.ggcode;
        $scope.redeem();
    }

    $scope.newUser = function () {
        $scope.showQuickSignUp = true;
    }
    $scope.existingUser = function() {
        $scope.showQuickSignin = true;
    }

    // QUICK SIGNIN
    $scope.quickIsDisabled = function() {
        return !$scope.qkUsername || !$scope.qkUsername.length || !$scope.qkPassword || !$scope.qkPassword.length;
    };
    $scope.quickSignUpDisabled = function() {
        return !$scope.suUsername || !$scope.suUsername.length || !$scope.suPassword || !$scope.suPassword.length;
    }
    $scope.quickSignIn = function() {
        $scope.qkError = null;
        api.account.login({
            'username': $scope.qkUsername, 
            'password': $scope.qkPassword,
            'code': $scope.punchCode
        }).then(function(response) {
            if (!response.data.user || !response.data.user.id) {
                $scope.qkError = response.data.errorDesc;
                return;
            }

            $scope.qkUsername = null;
            $scope.qkPassword = null;
            $scope.showQuickSignUp = false;
            $scope.yellowDialog = false;
            $scope.punchCode = null;
            $scope.user = response.data.user;
            $rootScope.$broadcast('loggedIn', response.data);
            if (params.ggcode) {
                $location.url($location.path());
            }
        });
    }
    $scope.quickSignUp = function() {
        $scope.suError = null;
        api.account.create({ 
            username:$scope.suUsername,
            password:$scope.suPassword,
            code:$scope.punchCode
        }).then(function(response) {
            if (!response.data.user || !response.data.user.id) {
                $scope.suError = response.data.errorDesc;
                return;
            }
            $scope.suUsername = null;
            $scope.suPassword = null;
            $scope.showQuickSignUp = false;
            $scope.yellowDialog = false;
            $scope.punchCode = null;
            $scope.user = response.data.user;
            $rootScope.$broadcast('loggedIn', response.data);
            if (params.ggcode) {
                $location.url($location.path());
            }
        });
    };
    $scope.$on('loggedIn', function(event, _data) {
        $scope.user = _data.user;
    });
    $scope.$on('loggedOut', function() {
        $scope.user = { id : 0 };
    });

    $scope.product = function() {
        $location.path('/product');
    }
}

function DefaultBrandCtrl ($rootScope, $scope, $location, api) {
    alert("brand default");
}

function ProductCtrl($rootScope, $scope, $location, api) {
    $scope.selectedNode = 0;
    $scope.details = function(node) {
        $scope.selectedNode = node;
    }
    $scope.signup = function() {
        $location.path('/join');
    }
}

function SignupCtrl($rootScope, $scope, $location, api) {
    $scope.email = '';
    $scope.username = '';
    $scope.password = '';
    $scope.password2 = '';
    $scope.businessName = '';
    $scope.subdomain = '';
    $scope.userType = '1';
    $scope.regFormValid = function() {
        return $scope.userType == '0' && $scope.regForm.$valid && $scope.username && $scope.password && $scope.password == $scope.password2;
    }
    $scope.businessFormValid = function() {

        return $scope.userType == '1' && $scope.businessName && $scope.email && $scope.firstName && $scope.lastName && $scope.businessForm.$valid && $scope.username && $scope.password && $scope.password == $scope.password2;
    }
    $scope.patterns = {
        email: /[a-zA-Z0-9._+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9]{2,4}/,
        username: /^[ ._a-zA-Z0-9]+$/,
        firstName: /^[ \'a-zA-Z0-9]+$/,
        lastName: /^[ \'a-zA-Z0-9]+$/,
        businessName: /^[!& \'a-zA-Z0-9]+$/
    }
    $scope.userTypeChanged = function() {
        //alert($scope.userType);
    }
    $scope.submit = function() {
        $scope.working = true;
        var options = {
            username:$scope.username,
            password:$scope.password
        };
        if ($scope.email && $scope.email.length) {
            options['email'] = $scope.email
        }   
        if ($scope.businessName && $scope.businessName.length) {
            options['businessName'] = $scope.businessName
        }        
        if ($scope.firstName && $scope.firstName.length) {
            options['firstName'] = $scope.firstName
        }
        if ($scope.lastName && $scope.lastName.length) {
            options['lastName'] = $scope.lastName
        }
        api.account.create(options).then(function(response) {
            $scope.working = false;
            if (response.data.errorDesc) {
                $scope.errorDesc = response.data.errorDesc;
                return;
            }

            response.data.cfurl = bomb.cfurl;
            bomb = response.data;
            var path = $location.path();
            if (!response.data.user.businessId && path != '/') {
                $scope.path = '/';  
            }
            else if (response.data.user.businessId && path != '/codes') {
                $scope.path = '/codes';
            }
            $location.path($scope.path);
            $rootScope.$broadcast('viewChanged', $scope.path);
            $scope.user = response.data.user;
            $rootScope.$broadcast('loggedIn', response.data);
        });
    }
}

function RewardsCtrl($scope, api, tableService, rewards) {
    //alert(JSON.stringify(rewards));
    $scope.rewards = rewards.data ? rewards.data : rewards; // TODO: weird.. figure out how to ge the response's data to become what is injected

    $scope.rewardTable = {
        pageNum: 1,
        resultsperpage: 5,
        sortAsc: false,
        sortCol: 'num_redeemed'
    };
    $scope.rewardTable = _.extend($scope.rewardTable, tableService);

    $scope.rewardTable.items = $scope.rewards;
    $scope.rewardTable.refreshPageNumberArray(5);

    $scope.create = function() {
        api.reward.create({
            name: $scope.newRewardName,
            desc: $scope.newRewardDesc,
            req_punches: $scope.nrReqPunches ? $scope.nrReqPunches : 1
        }).then(function(response) {
            response.data.num_redeemed = 0;
            $scope.rewards.push(response.data);
            $scope.rewardTable.refreshPageNumberArray(5);
            $scope.newRewardName = '';
            $scope.newRewardDesc = '';
            $scope.nrReqPunches = '';
        });
    }

    $scope.patterns = {
        rewardName: /^[ :;'"\{\}|\[\]\/,+=\?*%$#@!`~()\-&._a-zA-Z0-9]+$/,
        rewardDesc: /^[ :;'"\{\}|\[\]\/,+=\?*%$#@!`~()\-&._a-zA-Z0-9]+$/,
        requiredPunches: /^\d{1,3}$/
    }

    $scope.isdisabled = function() {
        return !$scope.newRewardName || !$scope.newRewardDesc || !$scope.rewardForm.$valid;
    }
}

// TODO: this Ctrl should probably be split into two, it is pretty fat
function CodeSearchCtrl($scope, $location, $filter, api, tableService) {
    $scope.codeType = 1;
    $scope.batchFilter = {
        showUnclaimed:true,
        showClaimed:true,
        showRedeemed:true,
        toggle:function(statusId) {
            switch(statusId) {
                case 1: this.showUnclaimed=!this.showUnclaimed; break;
                case 2: this.showClaimed=!this.showClaimed; break;
                case 4: this.showRedeemed=!this.showRedeemed; break;
            }
            $scope.codes = $filter('codeStatus')($scope.batchTable.codesInBatch, this.showUnclaimed, this.showClaimed, this.showRedeemed);
        }
    }
    $scope.showUnclaimed = true;
    $scope.showClaimed = true;
    $scope.showRedeemed = true;

    // TODO: convert these tables to an object, ie., $scope.batchTable = new Table()
    $scope.batchTable = {
        selectedBatchID: null,
        pageNum: 1,
        resultsperpage: 3,
        sortAsc: false,
        items: $scope.batches,
        sortCol: 'created',
        codesInBatch: [],
        rowSelected: function (batch, index) {
            this.selectedBatchID = batch.batchId;
            api.code.search({
                batchId: batch.batchId
            }).then(function (response) {
                $scope.showUnclaimed = true;
                $scope.showClaimed = true;
                $scope.showRedeemed = true;
                $scope.batchTable.codesInBatch = response.data;
                $scope.codes = $filter('codeStatus')($scope.batchTable.codesInBatch, $scope.batchFilter.showUnclaimed, $scope.batchFilter.showClaimed, $scope.batchFilter.showRedeemed);
            });
        }
    };
    $scope.batchTable = _.extend($scope.batchTable, tableService);
    $scope.batchTable.refreshPageNumberArray(3);

    var qrcode = null;
    $scope.codeTable = {
        selectedCodeID: null,
        pageNum: 1,
        resultsperpage: 10,
        sortAsc: false,
        items: [],
        sortCol: 'created',
        rowSelected: function (code) {
            $scope.codeUrl = code.code + '.ggco.de';
            if (!qrcode) {
                qrcode = new QRCode("qr", {
                    width: 256,
                    height: 256,
                    //colorDark : "#000000",
                    //colorLight : "#ffffff",
                    correctLevel : QRCode.CorrectLevel.H
                });
            }
            this.selectedCodeID = code.id;
            qrcode.makeCode(code.code + '.grubgrabr.com');
        }
    };
    $scope.codeTable = _.extend($scope.codeTable, tableService);

    $scope.viewCodeTypeChanged = function() {
        if ($scope.codeType == 1) {
            $scope.batchTable.selectedBatchID = null;
            $scope.batchTable.codesInBatch = [];
            //$scope.codes = [];
        }
        else if ($scope.codeType == 2) {

            api.code.search({

            }).then(function(response) {
                $scope.codes = response.data;
                $scope.refreshTableData();
            });
        }
    }
    $scope.toggle = function(status) {
        switch (status) {
            case 1: $scope.showUnclaimed = !$scope.showUnclaimed; break;
            case 2: $scope.showClaimed = !$scope.showClaimed; break;
            case 4: $scope.showRedeemed = !$scope.showRedeemed; break;
        }
        this.refreshTableData();
    }
    $scope.refreshTableData = function() {
        $scope.codeTable.items = $filter('codeStatus')($scope.codes, $scope.showUnclaimed, $scope.showClaimed, $scope.showRedeemed);
        $scope.codeTable.refreshPageNumberArray(10);
    }

    $scope.print = function(batch) {
        // batch.batchId

    }

    $scope.$on('batchCreated', function(event, data) {
        $scope.batches.unshift(data);
        $scope.batchTable.refreshPageNumberArray(3);
    });
    $scope.$on('ahcodeCreated', function(event, data) {
        data.id = data.codeId;
        if ($scope.codes) {
            $scope.codes.unshift(data);
            $scope.refreshTableData();
        }
    });
}

function CodesCtrl($scope, $location, api, batches, paymentinfo, code_stats) {
    $scope.paymentinfo = paymentinfo.data ? paymentinfo.data : paymentinfo;
    $scope.code_stats = code_stats.data ? code_stats.data : code_stats;
    $scope.batches = batches.data ? batches.data : batches; 
    $scope.newCodeType = 'random';
    $scope.tabs = {
        active: 'View',
        nodes: [
            { title: 'View' },
            { title: 'Generate' },
            { title: 'Redeem' },
            { title: 'Buy' }
        ],
        action: function (node) {
            this.active = node.title;
        }
    };
    $scope.availCodesDesc = function() {
        var sum =   ($scope.code_stats.num_permanent) + 
                    ($scope.code_stats.num_permanent_rr) +
                    $scope.code_stats.num_subscription + $scope.code_stats.num_subscription_rr;
        return sum.toLocaleString() + ' Grubcodes';
    }

    // yeah........ refactor these $ons
    $scope.$on('batchCreated', function(event, data) {
        updateStats(data);
    });
    $scope.$on('ahcodeCreated', function(event, data) {
        updateStats(data);
    });
    $scope.$on('purchasedCodes', function(event, data) {
        updateStats(data);
    });
    function updateStats(stats) {
        $scope.code_stats.num_permanent = stats.num_permanent;
        $scope.code_stats.num_permanent_rr = stats.num_permanent_rr;
        $scope.code_stats.num_subscription = stats.num_subscription;
        $scope.code_stats.num_subscription_rr = stats.num_subscription_rr;
    }
}

function CodeCreateCtrl($rootScope, $scope, $location, api) {
    $scope.codeType = 'random';
    $scope.quickNumPunches = 1;

    var quickQR = null;
    $scope.generate = function() {
        api.code.create({
            codeType: $scope.codeType == 'random' ? 1 : 2,
            code: $scope.customCode,
            punches: $scope.quickNumPunches,
            transferable: $scope.transferable ? 1 : 0,
            ratingsAndReviews: $scope.ratingsReviews ? 1 : 0
        }).then(function(response) {
            if (!quickQR) {
                quickQR = new QRCode("quickqr", {
                    width: 256,
                    height: 256,
                    //colorDark : "#000000",
                    //colorLight : "#ffffff",
                    correctLevel : QRCode.CorrectLevel.H
                });
            }
            var c = response.data['code'];
            $scope.quickCodeUrl = c + '.ggco.de';  
            $scope.generatedCode = c;
            quickQR.makeCode(c + '.grubgrabr.com');
            $rootScope.$broadcast('ahcodeCreated', response.data);
        });
    }
    $scope.setQuickPunches = function(_num) {
        $scope.quickNumPunches = _num;
    }
    $scope.create = function() {
        $scope.successMsg = null;
        $scope.errorMsg = null;
        api.code.create({
            codeType: $scope.codeType == 'random' ? 1 : 2,
            code: $scope.customCode,
            punches: $scope.punches,
            active: $scope.activeDate,
            expires: $scope.expireDate,
            quantity: $scope.quantity,
            transferable: $scope.transferable ? 1 : 0,
            ratingsAndReviews: $scope.ratingsReviews ? 1 : 0
        }).then(function(response) {
            if (response.data.Result != 0) {
                // failure
                $scope.errorMsg = response.data.errorDesc;
                return;
            }

            $scope.successMsg = "You've created " + (response.data.num_codes ? response.data.num_codes : 1) + " Grubcode" + (response.data.num_codes > 1 ? "s" : "")+".";
            $rootScope.$broadcast('batchCreated', response.data);
        });
    };
    $scope.patterns = {
        quantity: /^\d{1,4}$/,
        punches: /^\d{1,3}$/,
        customCode: /^[a-zA-Z0-9]+$/
    }
    $scope.codeTypeChanged = function() {
        if (!$scope.genForm.customCode.$valid) {
            $scope.customCode = null;
        }
    };

    $scope.isdisabled = function() {
        return !$scope.genForm.$valid;
    }

    $scope.rrCheckboxDisabled = function() {
        return $scope.code_stats.num_permanent_rr == 0 && $scope.code_stats.num_subscription_rr == 0;
    }
    $scope.rrChecked = function() {
        return $scope.code_stats.num_permanent_rr > 0 || $scope.code_stats.num_subscription_rr > 0;
    }
    $scope.ratingsReviews = $scope.rrChecked() ? "true" : "";
}

function CodeRedeemRewardCtrl($scope, api) {
    var _success = "Success!  This RewardCode has been redeemed.";
    var _verified = "This is a valid Rewardcode.  To redeem, press the button below.";
    var _invalid = "This Rewardcode is invalid.";
    var _already = "This Rewardcode was already redeemed on ";

    $scope.justChecking = true;
    $scope.feedbackBubble = {
        text: null,
        type: null
    };

    $scope.verify = function() {
        api.code.redeem({
            rewardCode: $scope.rewardCode,
            justChecking: 1
        }).then(function (response) {
            if (!response.data || !response.data.length)
                return;


            // TODO: put this shit somewhere else
            switch(response.data[0].Result) {
                case '0':
                    $scope.feedbackBubble.text = _verified;
                    $scope.feedbackBubble.type = 1;
                    break;
                case '1':
                    $scope.feedbackBubble.text = _invalid;
                    $scope.feedbackBubble.type = 2;
                    break;
                case '2':
                    $scope.feedbackBubble.text = _already + moment(response.data[0].RedeemedOn, 'YYYY-MM-DD HH:mm:ss').format('MMM D, YYYY h:mma');
                    $scope.feedbackBubble.type = 2;
                    break;
            }
        });
    }
    $scope.redeem = function() {
        api.code.redeem({
            rewardCode: $scope.rewardCode
        }).then(function (response) {
            if (!response.data || !response.data.length)
                return;

            switch(response.data[0].Result) {
                case '0':
                    $scope.feedbackBubble.text = _success;
                    $scope.feedbackBubble.type = 0;
                    break;
                case '1':
                    $scope.feedbackBubble.text = _invalid;
                    $scope.feedbackBubble.type = 2;
                    break;
                case '2':
                    $scope.feedbackBubble.text = _already + moment(response.data[0].RedeemedOn, 'YYYY-MM-DD HH:mm:ss').format('MMM D, YYYY h:mma');
                    $scope.feedbackBubble.type = 2;
                    break;
            }
        });
    };
    $scope.closeInfo = function (e) {
        $scope.feedbackBubble.text = null;
    }
    $scope.isDisabled = function() {
        return !$scope.rewardCode || $scope.rewardCode.length != 6;
    }
}

function CodeBuyCtrl($rootScope, $scope, $location, stripe, api) {
    $scope.purchaseType = null;
    $scope.chargedMeChecked = false;
    $scope.purchasePriceLabels = [10, 15, 20, 30];
    $scope.purchaseTypeChanged = function(purchaseType) {
        $scope.purchaseType = purchaseType;
        $scope.priceLabel = $scope.purchasePriceLabels[parseInt($scope.purchaseType)];
    }
    $scope.submitPayment = function() {
        api.account.submitPayment({
            paymentInfoId: $scope.selectedPayId,
            planId: $scope.purchaseType
        }).then(function(response) {
            if (!response.data || response.data.Result != 0) {
                return;
            }
            $scope.successDialog = true;
            $rootScope.$broadcast('purchasedCodes', response.data);
            //alert(JSON.stringify(response));
        });
    }

    $scope.selectMeth = function(cardId) {
        $scope.selectedPayId = cardId;
    }
    $scope.gotoaccount = function() {
        $location.path('/account');
        $rootScope.$broadcast('viewChanged', '/account');
    }

    
}

function AccountCtrl($scope, api, stripe, user, paymentinfo) {
    $scope.user = user.data ? user.data : user;
    $scope.paymentinfo = paymentinfo.data ? paymentinfo.data : paymentinfo;
    //$scope.number = '4242424242424242';
    $scope.patterns = {
        expireMonth: /[01]\d/,
        expireYear: /\d\d/,
        number: /\d+/,
        CVV: /\d+/
    }
    $scope.saveCard = function() {
        $scope.striping = true;
        $scope.errorText = null;
        stripe.tokenize($scope.number, $scope.CVV, $scope.expireMonth, $scope.expireYear, function(status, response) {
            if (response.error) {
                $scope.errorText = response.error.message;
                $scope.$digest();
                $scope.striping = false;
                return;
            }
            $scope.number = null;
            $scope.CVV = null;
            $scope.expireMonth = null;
            $scope.expireYear = null;

            api.account.addCard({
                stripeToken: response.id,
                card: response.card
            }).then(function(response) {
                $scope.paymentinfo.push(response.data);
                $scope.striping = false;
            });
        });
    }

    $scope.removeMeth = function(card) {
        api.account.delete_paymentinfo({
            id: card.id
        }).then(function(response) {
            $scope.paymentinfo = _.filter(paymentinfo, function(c) {
                return c.id != card.id;
            });
            bomb.paymentinfo = $scope.paymentinfo;
        });
    }

    $scope.addCardDisabled = function() {
        return $scope.striping || !stripe.validateCardNumber($scope.number) || !stripe.validateExpiry($scope.expireMonth, '20' + $scope.expireYear)
                || !stripe.validateCVC($scope.CVV);
    }
}

function PrintCtrl($scope, api) {
    $scope.codes = bomb.codes;
    $scope.qrCodify = function() {
        _.each($scope.codes, function(code) {
            var qrcode = new QRCode(code.id+'', {
                width: 256,
                height: 256,
                //colorDark : "#000000",
                //colorLight : "#ffffff",
                correctLevel : QRCode.CorrectLevel.H
            });
            qrcode.makeCode(code.code + '.grubgrabr.com');
        })
    }
}

function FeedbackCtrl($scope, tableService, api, feedback) {
    $scope.feedback = feedback.data ? feedback.data : feedback;
    $scope.cfurl = bomb.cfurl;
    $scope.feedbackTable = {
        pageNum: 1,
        resultsperpage: 5,
        sortAsc: false,
        sortCol: 'created'
    };
    $scope.feedbackTable = _.extend($scope.feedbackTable, tableService);
    $scope.feedbackTable.items = $scope.feedback.feedback;
    $scope.feedbackTable.refreshPageNumberArray(5);
}

function FeedbackModalCtrl($rootScope, $scope, api) {
    $scope.code = bomb.punchCode;

    if (typeof $scope.code !== 'undefined') {

        // TODO: this isn't DRY, the same code is below, fix this
        $('#imageF').fileupload({
            dataType: 'json',
            url: '/upload/image/' + $scope.code.code
        });
        $('#imageF').bind('fileuploadadd', function(e, _data) {
            $scope.uploading = true;
            _data.url = '/upload/image/' + $scope.code.code;
            _data.submit();
            $scope.$apply();
        });
        $("#imageF").bind("fileuploaddone", function(e, _data) {
            $scope.thumbUrl = bomb.cfurl + '/thumb/' + _data.result.key;
            $scope.code.imageKey = _data.result.key;
            $scope.uploading = false;
            $scope.$apply();
        });
    }

    $scope.submitrr = function() {
        api.code.submitRating({
            code: $scope.code.code,
            rating: $scope.code.inputRating,
            comment: $scope.code.inputComment
        }).then(function(response) {
            if (response.data.Result == 2) {
                return;
            }
            $scope.code.rating = $scope.code.inputRating;
            $scope.code.comment = $scope.code.inputComment;

            if (response.data.Result == 0) {
                $scope.code.num_punches = parseInt($scope.code.num_punches) + 1;
                $rootScope.$broadcast('ratingSubmitted', response.data);
            }

            $('#feedbackDialog').modal('hide');
        });
    }

    $rootScope.$on('showFeedback', _showFeedback);
    function _showFeedback(event, data) {
        $scope.code = data;
        $scope.thumbUrl = $scope.code.imageKey ? bomb.cfurl + '/thumb/' + $scope.code.imageKey : null;

        // initialize file upload
        $('#imageF').fileupload({
            dataType: 'json',
            url: '/upload/image/' + $scope.code.code
        });

        // bind file add event
        $('#imageF').unbind('fileuploadadd');
        $('#imageF').bind('fileuploadadd', function(e, data) {
            $scope.uploading = true;
            data.url = '/upload/image/' + $scope.code.code;
            data.submit();
            $scope.$apply();
        })

        // bind file upload done event
        $('#imageF').unbind('fileuploaddone');
        $('#imageF').bind('fileuploaddone', function (e, _data) {
            $scope.thumbUrl = bomb.cfurl + '/thumb/' + _data.result.key;
            $scope.code.imageKey = _data.result.key;
            $scope.uploading = false;
            $scope.$apply();
        });

        // set up and show dialog
        $scope.code.inputRating = $scope.code.rating;
        $scope.code.inputComment = $scope.code.comment;
        setTimeout(function() {
            $('#feedbackDialog').modal({ show: true });
        }, 0);
    }
}

function ViewFeedbackCtrl($scope) {
    //$scope = bomb;
    $scope.code = bomb.code;
    $scope.cfurl = bomb.cfurl;
    //alert(JSON.stringify($scope));
}

