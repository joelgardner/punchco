a) generate a punchcode that will be displayed on receipt @ the time of purchase (or they can print and cut little strips).  say the code is a3f8fn.
	the code must be de-activated in 7 days or 
b) go to elevation.punchco.de/a3f8fn (also elevation.punchco.com/a3f8fn)
	- each customer has a subdomain ie., Elevation Burger's might be elevation
	- the page asks you to supply an identifier string (could be email, username etc.  this is the link between the user and his free shit), it must be the same for every punchcode he receives
	- once this is done, the page will indicate taht this code has been de-activated and added to the identifier string's "code list"
	- the user obtains enough codes (or enough "units" via the codes if codes can be assigned multiple units), and he can redeem them for another code
		that will get the user some free shit or whatever
	- 


punchcode lifecycle:
	a) code is generated, displayed on receipt
		- ACTIVE
	b) code is applied to an identifier string
		- INACTIVE

enough codes will generate a redeemcode

redeemcode lifecycle:
	a) code is generated, and assigned to a reward (rewards could be anything: single (7 units), single with cheese (8 units), double (12 units), double with cheese (13 units))
		- ACTIVE
	b) code is redeemed at brick&mortar location, employee can crosscheck list of offline (assuming the offline source is updated every day) or use online api to verify code is active
		- INACTIVE

codes will be randomly generated alpha-numeric strings from a pool of (26 letters + 10 numerals) = 36 characters
36 ^ 2 = 1,296
36 ^ 3 = 46,656
36 ^ 4 = 1,679,616
36 ^ 5 = 60,466,176				==> 0.000000017
36 ^ 6 = 2,176,782,336			==> 0.00000000046
36 ^ 7 = 78,364,164,096			==> 0.000000000012 -- odds of "guessing" a correct code
36 ^ 8 = 2,821,109,907,456		==> 0.00000000000035

36 ^ 7 = 78,364,164,096
78,364,164,096 / 5,000 = 15,672,832 months of businesses using all 5,000 codes
15,672,832 / 12 months = 1 year's worth of 5,000 codes per month for 1,306,069 businesses

abcdefghijkmnopqrstuvwxyz
ABCDEFGHJKLMNPQRSTUVWXYZ

elevationburger.punchco.de/1f8di9f
starbucks.punchco.de/384fndx
starbucks.punchcode.com/384fndx

the sell:
a) add subdomain and 1000 codes
b) provide manager with login credentials
c) allow manager to set up rewards (maybe set up a basic reward to show him how)

as the account manager of elevation burger, i go to punchco.com and sign up, creating Elevation Burger's account and subdomain
- i create rewards and assign them a unit value.
- i generate 1000 codes (perhaps there's a pool of unused codes to pull from) valued at 1 unit, and 1000 codes valued at 2 unit, and download them as a .CSV, figure out a distribution method
- employees hand them out until they run out, repeat.
- verify a redeemcode on website.

a) set up punchco mysql user:
	- /usr/local/mysql/bin/mysql --user=root -p
	- grant all privileges on punchco.* to 'punchco'@'localhost';
	- create user 'punchco'@'localhost' identified by 'h3y';
b) update hosts file:
	- /private/etc/hosts
c.0) start mysqld:
	- sudo /usr/local/mysql/bin/mysqld_safe (CTRL+Z)
c.1) stop mysqld:
	- sudo /usr/local/mysql/bin/mysqladmin -u root -p shutdown
d) update http .conf files:
	- /private/etc/apache2/httpd.conf (example: LoadModule php5_module /opt/local/apache2/modules/mod_php54.so)
	- /private/etc/apache2/extra/httpd-vhosts.conf (add a vhost like so:)
		<VirtualHost *:80>
		    DocumentRoot "/Users/jroot/Sites/punchco/www"
		    ServerName grubgrabr.com
		    SetEnv LIVENESS dev
		    ServerAlias *.grubgrabr.com
		    #ErrorLog "/private/var/log/apache2/punchco-error_log.log"
		    #CustomLog "/private/var/log/apache2/punchco-access_log.log" common

		    <Directory "/Users/jroot/Sites/punchco/www">
		        AllowOverride All
		        Order allow,deny
		        Allow from all
		    </Directory>
		</VirtualHost>
*** NOTE: YOU MUST ADD A "SetEnv LIVENESS dev|test|live" LINE TO THE VHOST FILE ***

e) set up log file permissions:
	- sudo chmod 666 log/server.log
f) deploy common_schema to mysql server
	- sudo /usr/local/mysql/bin/mysql -p < /Users/jroot/Downloads/common_schema-2.2.sql
g) copy my.cnf to /etc/my.cnf

grubco.de/ausdux8
grubcode.com/ausdux8
abcd123.grubco.de
ausdux8.grubcode.com



rackspace: S1
Sm8SsLaxRMUU


1.a) make sure /etc/httpd/conf/httpd.conf has the correct DocumentRoot (/var/www/html/www) and <VirtualHost> node (it can replace the default <Directory> node):
	<VirtualHost *:80>
	    DocumentRoot "/var/www/html/www"
	    ServerName grubgrabr.com
	    ServerAlias *.grubgrabr.com
	    ErrorLog "/var/log/grubgrabr-error_log.log"
	    CustomLog "/var/log/grubgrabr-access_log.log" common
	    <Directory "/var/www/html/www">
	        Options FollowSymLinks
	        AllowOverride All
	        Order allow,deny
	        Allow from all
	        RewriteEngine On
	        RewriteCond %{REQUEST_FILENAME} !-f
	        RewriteRule ^ index.php [QSA,L]
	    </Directory>
	</VirtualHost>

1.b) set in php.ini:
upload_max_filesize = 3M

2) sudo chmod 666 /var/www/html/log/server.log
3) if an error about /var/lib/php/session being inaccessible use:
	sudo chown webapp.webapp /var/lib/php/session

4) in AWS RDS: make sure log_bin_trust_function_creators is set to 1 (ON) and that the db is using that specific parameter group
also when installing db make sure it's create DEFINER=CURRENT_USER procedure/function blah

5) 
 
SSH into beanstalk instance:
ssh -i ../aws/Joel.pem ec2-user@ec2-54-200-180-216.us-west-2.compute.amazonaws.com