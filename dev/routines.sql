SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
CREATE SCHEMA IF NOT EXISTS `punchco` DEFAULT CHARACTER SET latin1 ;
USE `mydb` ;
USE `punchco` ;

-- -----------------------------------------------------
-- procedure add_user
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `add_user`(
	_email varchar(150),
	_username varchar(50),
	_passwordHash varchar(100),
	_businessName varchar(45),
	_firstName varchar(45),
	_lastName varchar(45)
)
begin
	declare userId,
			SUCCESSFUL_ADD,
			EMAIL_ALREADY_IN_USE,
			USERNAME_ALREADY_IN_USE,
			BUSINESSNAME_ALREADY_IN_USE int;

	set SUCCESSFUL_ADD = 0,
		EMAIL_ALREADY_IN_USE = 1,
		USERNAME_ALREADY_IN_USE = 2,
		BUSINESSNAME_ALREADY_IN_USE = 3;

	if exists (select 1 from user where email=_email) then
		select EMAIL_ALREADY_IN_USE as Result, 'The provided email is already in use.' as ErrorDesc;
	elseif exists (select 1 from user where `name`=_username) then
		select USERNAME_ALREADY_IN_USE as Result, 'The provided username is already in use.' as ErrorDesc;
	elseif _businessName is not null and exists (select 1 from business where `name`=_businessName) then
		select BUSINESSNAME_ALREADY_IN_USE as Result, 'The provided business name is already in use.' as ErrorDesc;
	else

		insert user (email, `name`, passwordHash, firstName, lastName, registerDate, lastChangeDate)
		select _email, _username, _passwordHash, _firstName, _lastName, current_timestamp, current_timestamp;

		set userId = last_insert_id();

		if _businessName is not null then
			insert business (`name`, accountOwnerId, num_subscription)
			select _businessName, userId, 250;
		end if;

		select SUCCESSFUL_ADD Result, u.*, b.*
		from user u
			left join business b on u.id=b.accountOwnerId
		where u.id = userId;
	
	end if;

end$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure addoredit_reward
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `addoredit_reward`(_id int, _businessId int, _creatorId int, _name varchar(256),
_desc varchar(1024), _reqPunches int)
BEGIN
	
	if _id is null or _id = 0 then
		insert reward (businessId, creatorId, name, `desc`, required_punches, changed)
		select _businessId, _creatorId, _name, _desc, _reqPunches, current_timestamp;

		set _id = last_insert_id(); 
	else
		update reward
		set businessId = _businessId,
			creatorId = _creatorId,
			name = _name,
			`desc` = _desc,
			required_punches = _reqPunches,
			changed = current_timestamp
		where id = _id;
	end if;

	select *
	from reward
	where id = _id;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure code_create
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `code_create`(_businessID int, _jsonOptions text)
BEGIN

	declare codeType, punches, quantity, _batchId, i, 
		num_to_remove_from_subscription_pool, 
		num_to_remove_from_permanent_pool int;
	declare activeDate, expireDate datetime;
	declare transferable, ratingsAndReviews int;
	declare c varchar(64);

	-- codeType
	set codeType = common_schema.get_option(_jsonOptions, 'codeType');
	if codeType is null then
		set codeType = 1;
	end if;

	-- punches
	set punches = common_schema.get_option(_jsonOptions, 'punches');
	if punches is null then
		set punches = 1;
	end if;

	-- quantity
	set quantity = common_schema.get_option(_jsonOptions, 'quantity');
	if quantity is null or quantity < 1 then
		set quantity = 1;
	end if;

	-- activeDate
	set activeDate = common_schema.get_option(_jsonOptions, 'active');

	-- expireDate
	set expireDate = common_schema.get_option(_jsonOptions, 'expires');

	-- transferable
	set transferable = common_schema.get_option(_jsonOptions, 'transferable');
	if transferable is null then
		set transferable = 1;
	end if;

	-- ratingsAndReviews
	set ratingsAndReviews = common_schema.get_option(_jsonOptions, 'ratingsAndReviews');
	if ratingsAndReviews is null then
		set ratingsAndReviews = 0;
	end if;

	-- num_to_remove_from_subscription_pool
	set num_to_remove_from_subscription_pool = common_schema.get_option(_jsonOptions, 'num_to_remove_from_subscription_pool');
	if num_to_remove_from_subscription_pool is null then
		set num_to_remove_from_subscription_pool = 0;
	end if;

	-- num_to_remove_from_permanent_pool
	set num_to_remove_from_permanent_pool = common_schema.get_option(_jsonOptions, 'num_to_remove_from_permanent_pool');
	if num_to_remove_from_permanent_pool is null then
		set num_to_remove_from_permanent_pool = 0;
	end if;

	-- assign a batchID when quantity > 1
	if quantity > 1 then
		set _batchId = ifnull((select max(batchId) + 1 from `code`), 1);
	end if;

	set i = 0;
	while i < quantity do
		set c = randomString(7);
		while exists (select 1 from code where code = c) do 
			set c = randomString(7);
		end while;

		insert code (`code`, businessId, typeId, statusId, num_punches, active, expire, batchId, transferable, ratings_and_reviews, changed)
		select c, _businessId, codeType, 1, punches, activeDate, expireDate, _batchId, transferable, ratingsAndReviews, current_timestamp;

		set i = i + 1;
	end while;
		
	-- update business.num_*
	if ratingsAndReviews = 1 then
		update business
		set 
			num_subscription_rr = num_subscription_rr - num_to_remove_from_subscription_pool,
			num_permanent_rr = num_permanent_rr - num_to_remove_from_permanent_pool
		where id = _businessId;
	elseif ratingsAndReviews = 0 then
		update business
		set 
			num_subscription = num_subscription - num_to_remove_from_subscription_pool,
			num_permanent = num_permanent - num_to_remove_from_permanent_pool
		where id = _businessId;
	end if;

	-- return result
	if quantity = 1 then
		select 0 as Result, code.id codeId, code, businessId, b.name businessName, statusId, num_punches, ratings_and_reviews, code.created, 
			b.num_permanent_rr, b.num_permanent, b.num_subscription_rr, b.num_subscription
		from `code`
			join business b on code.businessId = b.id
		where code = c;
	else
		select 0 as Result, batchId, code.created, num_punches, transferable, ratings_and_reviews, count(1) num_codes, 
			b.num_permanent_rr, b.num_permanent, b.num_subscription_rr, b.num_subscription
		from code
			join business b on code.businessId = b.id
		where batchId = _batchId;
	end if;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure code_recentactivity
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `code_recentactivity`(_userId int)
BEGIN

	select c.id codeId, code, businessId, b.Name BusinessName, statusId, cs.Name statusName, num_punches, punched, 
		brr.rating, brr.comment, brr.created rrCreated
	from `code` c
		join business b on c.businessId=b.id
		join codeStatus cs on c.statusId=cs.Id
			left join businessratingsreviews brr on c.id = brr.codeId	
	where c.userId = _userId
	order by punched desc
	limit 100;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure code_search
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `code_search`(_businessID int, _jsonOptions text)
BEGIN

	declare _batchId, _pageNum, _pageSize int;
	declare activeDate, expireDate datetime;
	declare transferable bit;
	declare c varchar(64);

	-- batchId
	set _batchId = common_schema.get_option(_jsonOptions, 'batchId');
	-- if _batchId is null then
	-- 	set _batchId = null;
	-- end if;
	
/*
	-- page number (only matters if batchId is null)
	set _pageNum = common_schema.get_option(_jsonOptions, 'pageNum');
	if _pageNum is null or _pageNum <= 0 then
		set _pageNum = 1;
	end if;

	-- page size
	set _pageSize = common_schema.get_option(_jsonOptions, 'pageSize');
	if _pageSize is null or _pageSize <= 0 then
		set _pageSize = 10;
	elseif  _pageSize > 50 then
		set _pageSize = 50;
	end if;
*/
	
	select c.*, brr.rating, brr.comment, brr.created rrCreated
	from code c
		left join businessratingsreviews brr on c.id = brr.codeId
	where c.businessId = _businessID 
		and (c.batchId = _batchId or (_batchId is null and c.batchId is null))
	order by c.created desc
	limit 100;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure create_batch
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `create_batch`(_businessId int, _desc varchar(45), num_codes int, num_punches int)
BEGIN
	declare batchID int;
	declare i int;

	insert code_batch (`desc`, businessId, num_punches)
	select _desc, _businessId, num_punches;
	set batchID = last_insert_id();

	set i =0;
	while i < num_codes
	do
		call generate_code(_businessId, num_punches, batchID);
		set i = i + 1;
	end while;

	select *
	from code_batch
	where id = batchID;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure delete_paymentinfo
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `delete_paymentinfo`(_businessId int, _paymentinfoId int)
BEGIN

	if not exists (select 1 from businesspurchases where paymentinfoId = _paymentinfoId) then
		delete
		from businesspaymentinfo
		where id = _paymentinfoId and businessId = _businessId;
	else
		update businesspaymentinfo
		set `deleted` = 1
		where id = _paymentinfoId and businessId = _businessId;
	end if;
	select 0 as 'Result';

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure generate_code
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `generate_code`(businessId int, numPunches int, _batchId int)
BEGIN
	declare c varchar(64);
	set c = randomString(7);
	#select c;
	
	while exists (select 1 from code where code = c)
	do
		set c = randomString(7);
		#select c;
	end while;
	
	insert code (code, businessId, typeId, statusId, num_punches, batchId, changed)
	select c, businessId, 1, 1, numPunches, _batchId, current_timestamp;

	if _batchId is null or _batchId = 0
	then
		select c as `code`;
	end if;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure generate_rewardcode
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `generate_rewardcode`(
	_businessId int, 
	_userId int, 
	_rewardId int)
BEGIN
	declare c varchar(6);
	set c = randomString(6);
	
	while exists (select 1 from rewardcode where code = c)
	do
		set c = randomString(6);
	end while;
	
	-- first, we create a new rewardcode
	
	insert rewardcode (code, userId, businessId, statusId, rewardId, created)
	select c, _userId, _businessId, 2, _rewardId, current_timestamp
	from reward r
		join business b on r.businessId=b.id
	where b.id = _businessId and r.id = _rewardId;
		
	select c as `code`;
	
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure get_available_code_stats
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `get_available_code_stats`(_businessId int)
BEGIN
	declare 	totalPermanentNoRR, totalPermanentRR, 
				numUsedPermanentNoRR, numUsedPermanentRR, 
				totalCurrentSubscriptionNoRR, totalCurrentSubscriptionRR,
				numUsedCurrentSubscriptionNoRR, numUsedCurrentSubscriptionRR int;
/*
	-- get total of perm codes w/ NO ratings&reviews
	set totalPermanentNoRR = (
		select sum(num_codes) 
		from businesspurchases 
		where businessId = _businessId and ratings_and_reviews = 0 and subscription = 0
	);

	-- get total of perm codes w/ ratings&reviews
	set totalPermanentRR = (
		select sum(num_codes) 
		from businesspurchases 
		where businessId = _businessId and ratings_and_reviews = 1 and subscription = 0
	);

	-- get number of used perm codes w/ NO ratings&reviwes
	set numUsedPermanentNoRR = (
		select count(id) 
		from code 
		where businessId = _businessId and ratings_and_reviews = 0
	);

	-- get number of used perm codes w/ ratings&reviews
	set numUsedPermanentRR = (
		select count(id) 
		from code 
		where businessId = _businessId and ratings_and_reviews = 1
	);

	-- get total current subscription codes w/ NO ratings&reviews
	set totalCurrentSubscriptionNoRR = (
		select num_subscription
		from business 
		where id = _businessId
	);

	-- get total current subscription codes w/ ratings&reviews
	set totalCurrentSubscriptionRR = (
		select num_subscription_rr
		from business 
		where id = _businessId
	);
*/
	select num_permanent, num_permanent_rr, 
			num_subscription, num_subscription_rr
	from business
	where id = _businessId;
	
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure get_batches
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `get_batches`(
	_businessId int
)
begin

	select cb.id, cb.businessId, cb.num_punches, `desc`, cb.created, sum(ifnull(least(1, c.batchId), 0)) numCodes
	from code_batch cb
		left join `code` c on cb.Id = c.batchId
	where cb.businessId = _businessId
	group by cb.id, cb.businessId, `desc`, cb.created
	order by cb.created desc;

end$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure get_codes
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `get_codes`(
	_businessId int
)
BEGIN

	select batchId, created, num_punches, transferable, ratings_and_reviews, count(1) num_codes
	from code
	where businessId = _businessId and batchId is not null
	group by batchId
	order by created desc;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure get_codes_for_print
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `get_codes_for_print`(_businessId int, _batchId int)
BEGIN

	select id, code, num_punches, ratings_and_reviews as rr, transferable
	from code
	where businessId = _businessId and batchId = _batchId and statusId = 1;
 
END$$

DELIMITER ;

-- -----------------------------------------------------
-- function get_new_code
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER FUNCTION `get_new_code`() RETURNS varchar(7) CHARSET latin1
begin
	declare c varchar(7);
	set c = randomString(7);

	while exists (select 1 from code where code = c)
	do 
		set c = randomString(7);
	end while;
	
	return c;
end$$

DELIMITER ;

-- -----------------------------------------------------
-- function get_new_rewardcode
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER FUNCTION `get_new_rewardcode`() RETURNS varchar(6) CHARSET latin1
begin
	declare c varchar(6);
	set c = randomString(6);

	while exists (select 1 from rewardcode where code = c)
	do 
		set c = randomString(6);
	end while;
	
	return c;
end$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure get_payment_info
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `get_payment_info`(
	_businessId int
)
BEGIN

	select id, cardType, last4
	from businesspaymentinfo
	where businessId = _businessId and deleted = 0;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure get_paymentinfo_customerId
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `get_paymentinfo_customerId`(_businessId int, paymentInfoId int)
BEGIN

	-- businessId is passed in as a security measure.  it comes from the serverside session data
	-- and is used to guard against restaraunt A faking restaraunt B's paymentInfoId
	select stripeCustomerId
	from businesspaymentinfo
	where id = paymentInfoId and businessId = _businessId;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure get_rewards
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `get_rewards`(
	_businessId int
)
begin

	select r.*, count(rc.rewardId) num_redeemed
	from reward r
		left join rewardcode rc on r.id=rc.rewardId
	where r.businessId = _businessId
	group by r.id
	order by count(rc.rewardId) desc;

end$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure get_salt_by_username
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `get_salt_by_username`(
	in username varchar(50)
)
begin

	select substring(passwordHash, 1, 29) as `salt`
	from user
	where `name`=username;

end$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure get_user_rewards
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `get_user_rewards`(_userId int)
BEGIN
	drop temporary table if exists businesses;
	create temporary table businesses (
		id int,
		businessName varchar(256),
		totalPunches int,
		unusedPunches int,
		mostRecentRedeem datetime
	);

	insert businesses (id, businessName, totalPunches, mostRecentRedeem)
	select b.id, b.`name`, sum(c.num_punches), max(c.punched)
	from code c
		join business b on c.businessId=b.id
	where c.userId = _userId
	group by b.id;

	update businesses a
		join (
			select c.businessId, sum(c.num_punches) availablePunches
			from code c 
			where c.userId = _userId and c.statusId = 2
			group by c.businessId
		) b on a.id = b.businessId
	set unusedPunches = b.availablePunches;

	-- resultset 1: the businesses that the user has redeemed a code for
	select id businessId, businessName, totalPunches as businessScore, unusedPunches as availablePunches
	from businesses
	order by mostRecentRedeem desc;

	-- resultset 2: the rewards available 
	select r.id rewardId, b.id businessId, r.name rewardName, r.desc rewardDesc, r.required_punches
	from reward r
	join businesses b on r.businessId=b.id
	where r.businessId = b.id 
	order by r.businessId, r.required_punches desc;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure increase_permanent_codes
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `increase_permanent_codes`(_businessId int, numCodes int)
BEGIN

	update business
	set num_permanent_codes = num_permanent_codes + numCodes
	where id = _businessId;

	select num_permanent_codes, num_subscription_codes
	from business
	where id = _businessId;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure punchcode_redeem
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `punchcode_redeem`(_code varchar(7), _userId int)
begin
	declare 	SUCCESSFUL_REDEEM,
				INVALID_CODE,
				INVALID_USER int;

	set SUCCESSFUL_REDEEM = 0;
	set INVALID_CODE = 1;
	set INVALID_USER = 2;

	if _code is null or not exists (select 1 from code where `code` = _code and statusId = 1) then
		select INVALID_CODE as Result;
	elseif _userId is null or not exists (select 1 from user where id = _userId) then
		select INVALID_USER as Result;
	else
		update `code`
		set userId = _userId, statusId = 2, `changed` = current_timestamp, `punched` = current_timestamp
		where `code` = _code;

		select SUCCESSFUL_REDEEM as Result, c.id codeId, c.code, businessId, b.Name businessName, statusId, cs.Name statusName, num_punches, punched
		from `code` c
			join business b on c.businessId=b.id
			join codeStatus cs on c.statusId=cs.Id
		where c.code = _code;
	end if;

end$$

DELIMITER ;

-- -----------------------------------------------------
-- function randomString
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER FUNCTION `randomString`($length int) RETURNS varchar(128) CHARSET latin1
BEGIN

	SET @chars = '0123456789abcdefghijklmnopqrstuvwxyz';
	SET @charLen = length(@chars);

	SET @randomString = '';

	WHILE length(@randomString) < $length DO
		SET @randomString = concat(@randomString, substring(@chars,CEILING(RAND() * @charLen),1));
	END WHILE;

	RETURN @randomString ;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure reward_myrewards
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `reward_myrewards`(_userId int)
BEGIN

	select rc.id rewardCodeId, rc.code, rc.businessId, b.Name businessName, r.id rewardId, r.Name rewardName, r.desc rewardDesc, statusId, cs.Name statusName, required_punches, rc.created
	from rewardcode rc
		join business b on rc.businessId=b.id
		join codeStatus cs on rc.statusId=cs.Id
		join reward r on rc.rewardId = r.id
	where userId = _userId
	order by created desc;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure reward_redeem
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `reward_redeem`(_userId int, _rewardId int)
BEGIN
	declare current_sum, requiredPunches, _codeId, t, _businessId, _rewardCodeId, SUCCESSFUL_REDEEM, INSUFFICIENT_PUNCHES int;
	declare _rewardCode varchar(6);
	declare newPunchCode varchar(7);
	declare done boolean default false;
	declare code_cursor cursor for
	select id
	from punched_codes;
	declare continue handler for not found set done = true;

	set SUCCESSFUL_REDEEM = 0,
		INSUFFICIENT_PUNCHES = 1;

	-- create a temp table to hold candidate codes to be redeemed, largest (by num_punches) first
	drop temporary table if exists punched_codes;
	create temporary table punched_codes (
		id int,
		num_punches int,
		`use` bit default 0,
		statusId int
	);

	set requiredPunches = (select required_punches from reward where id = _rewardId);
	set _businessId = (select businessId from reward where id = _rewardId);

	-- TODO: this will break if a user has 100 codes worth 1 punch each and a reward costs >100 punches (remove limit)
	insert punched_codes (id, num_punches, statusId)
	select id, num_punches, 2
	from `code` c
	where c.userId = _userId 
		and c.businessId = _businessId
		and c.statusId = 2
	order by num_punches desc, `punched`
	-- limit 100
;

	set current_sum = 0;

	if ifnull((select sum(num_punches) from punched_codes), 0) < requiredPunches then
		select INSUFFICIENT_PUNCHES as Result, 'You do not have enough Punches' as Feedback;
	else
		-- update the minimum amount of codes necessary to get the reward
		open code_cursor;
		code_loop: loop
			fetch code_cursor into _codeId;

			-- exit
			if done or current_sum = requiredPunches then 
				leave code_loop; 
			end if;
			
			-- keep adding codes to redeem as long as the sum is never > requiredPunches
			set t = (select num_punches from punched_codes where id = _codeId);
			if current_sum + t <= requiredPunches then
				set current_sum = current_sum + t;

				update punched_codes
				set `use` = 1, 
					statusId = 4
				where id = _codeId;
			end if;
			
		end loop;
		close code_cursor;

		-- if loop didn't
		if current_sum < requiredPunches then

			-- increase the biggest used code (if any) by the difference
			set _codeId = (select id from punched_codes where `use` = 1 order by num_punches desc limit 1);
			update punched_codes
			set num_punches = num_punches + (requiredPunches - current_sum) 
			where id = _codeId;
		
			-- decrease the biggest unused code by the difference
 			set _codeId = (select id from punched_codes where `use` = 0 order by num_punches desc limit 1);
			update punched_codes
			set num_punches = num_punches - (requiredPunches - current_sum),
				`use` = 1
			where id = _codeId;

			-- sanity check, remove this eventually
			if (select sum(num_punches) from punched_codes where statusId = 4) <> requiredPunches then
				select 'something went wrong' as Result;
			end if;
		end if;

		-- delete from temp table the codes we aren't going to redeem
		delete 
		from punched_codes
		where `use` = 0;
		
		-- update the codes' status and create a new rewardcode
		update `code` c
			join punched_codes pc on c.id=pc.id
		set c.statusId = pc.statusId,
			c.rewardId = _rewardId,
			c.num_punches = pc.num_punches,
			c.changed = current_timestamp;

		-- create a new rewardcode
		set _rewardCode = get_new_rewardcode();
		insert rewardcode (userId, businessId, rewardId, `code`, statusId, created)
		select _userId, _businessId, _rewardId, _rewardCode, 2, current_timestamp;

		set _rewardCodeId = last_insert_id();

		-- resultset 1
		select SUCCESSFUL_REDEEM as Result, rc.id rewardCodeId, rc.code, rc.businessId, b.Name businessName, r.id rewardId, r.Name rewardName, r.desc rewardDesc, statusId, cs.Name statusName, required_punches, rc.created
		from rewardcode rc
			join business b on rc.businessId=b.id
			join codeStatus cs on rc.statusId=cs.Id
			join reward r on rc.rewardId = r.id
		where rc.id = _rewardCodeId;

		-- resultset 2
		select c.id, c.code, c.businessId, c.statusId, c.num_punches, c.created, c.punched, c.rewardId
		from punched_codes pc
			join code c on pc.id = c.id;

	end if;

	drop temporary table if exists punched_codes;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure rewardcode_redeem
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `rewardcode_redeem`(_rewardCode varchar(6), _jsonOptions text)
BEGIN
	declare INVALID_REWARDCODE, REWARDCODE_ALREADY_REDEEMED, SUCCESSFUL_REDEEM int;
	declare _justChecking bit;
	set SUCCESSFUL_REDEEM = 0,
		INVALID_REWARDCODE = 1,
		REWARDCODE_ALREADY_REDEEMED = 2;

	-- just checking (if true don't actually update rewardcode)
	set _justChecking = case when common_schema.get_option(_jsonOptions, 'justChecking') = '1' then 1 else 0 end;

	if exists (select 1 from rewardcode where code = _rewardCode and statusId = 2) then
		if _justChecking = 0 then
			update rewardcode
			set statusId = 4, redeemed = current_timestamp
			where code = _rewardCode and statusId = 2;
		end if;
		select SUCCESSFUL_REDEEM as `Result`;
	elseif (select 1 from rewardcode where code = _rewardCode) then
		select REWARDCODE_ALREADY_REDEEMED as `Result`,
			(select redeemed from rewardcode where code = _rewardCode) `RedeemedOn`;
	else
		select INVALID_REWARDCODE as `Result`;
	end if;
/*
	declare _requiredPunches, _userScore int;
	declare _code varchar(6);
	declare _error varchar(256);
	set _code = get_new_rewardcode();

	select _requiredPunches = require_punches
	from reward
	where businessId = _businessId;

	select _userScore = sum(num_punches)
	from code
	where userId = _userId and businessId = _businessId and statusId = 2;

	if _userScore >= _requiredPunches then
		reward
	end if;
*/
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure save_business_transaction
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `save_business_transaction`(
	_businessId int,
	_paymentInfoId int,
	_ratings_and_reviews int,
	_num_codes int,
	_subscription int,
	_charge_amount decimal(14, 2)
)
BEGIN
	declare subcodes int;
	insert businesspurchases (businessId, paymentinfoId, ratings_and_reviews, num_codes, subscription, charge_amount, created)
	select _businessId, _paymentInfoId, _ratings_and_reviews, _num_codes, _subscription, _charge_amount, current_timestamp;

	if _subscription = 0 and _ratings_and_reviews = 0 then
		update business
		set num_permanent = num_permanent + _num_codes
		where id = _businessId;
	elseif _subscription = 0 and _ratings_and_reviews = 1 then
		update business
		set num_permanent_rr = num_permanent_rr + _num_codes
		where id = _businessId;
	elseif _subscription = 1 and _ratings_and_reviews = 0 then
		update business
		set num_subscription = 5000
		where id = _businessId;
	elseif _subscription = 1 and _ratings_and_reviews = 1 then
		update business
		set num_subscription_rr = 5000
		where id = _businessId;
	end if;

	select 0 as Result, num_permanent, num_permanent_rr, num_subscription, num_subscription_rr
	from business
	where id = _businessId;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure save_payment_info
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `save_payment_info`(
	_businessId int,
	_stripeCustomerId varchar(256),
	_cardType varchar(16),
	_last4 varchar(4)
)
BEGIN

	insert businesspaymentinfo (businessId, stripeCustomerId, cardType, last4, created)
	select _businessId, _stripeCustomerId, _cardType, _last4, current_timestamp;
		
	select *
	from businesspaymentinfo
	where stripeCustomerId = _stripeCustomerId;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure save_rating
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `save_rating`(
	_userId int,
	_codeId int,
	_rating int,
	_comment varchar(2048)
)
BEGIN
	declare result, SUCCESS_SCORE_UPDATED, SUCCESS_SCORE_NOT_UPDATED,
			FAILURE int;
	set 
		SUCCESS_SCORE_UPDATED = 0,
		SUCCESS_SCORE_NOT_UPDATED = 1,
		FAILURE = 2;
	set result = FAILURE;
	if exists (select 1 from businessratingsreviews where codeId=_codeId) then
		update businessratingsreviews
		set rating = _rating, comment = _comment
		where codeId = _codeId and userId = _userId;

		set result = SUCCESS_SCORE_NOT_UPDATED;
	elseif exists (select 1 from code where id = _codeId and userId = _userId) then
		insert businessratingsreviews (codeId, userId, rating, comment, created)
		select _codeId, _userId, _rating, _comment, current_timestamp;
	
		update code
		set num_punches = num_punches + 1
		where id = _codeId;

		set result = SUCCESS_SCORE_UPDATED;
	end if;

	select result as Result, businessratingsreviews.*
	from businessratingsreviews
	where codeId=_codeId and userId = _userId; 
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure user_login
-- -----------------------------------------------------

DELIMITER $$
USE `punchco`$$
CREATE DEFINER=CURRENT_USER PROCEDURE `user_login`(username varchar(50), pwordHash varchar(100))
BEGIN
	declare _userId, totalScore, SUCCESSFUL_LOGIN, INVALID_LOGIN_CREDENTIALS int;
	set SUCCESSFUL_LOGIN = 0,
		INVALID_LOGIN_CREDENTIALS = 1;

	set _userId = (select id from user where `name`=username and passwordHash=pwordHash);
	
	if _userId > 0 then
		set totalScore = (select sum(num_punches) from code c where c.userId = _userId);
		
		update user
		set lastLoginDate = current_timestamp
		where user.id = _userId;

		select SUCCESSFUL_LOGIN as Result, u.id, u.email, u.name, u.firstName, u.lastName, u.registerDate, u.lastLoginDate, totalScore as score, b.id as businessId, b.accountOwnerId as ownerId, b.name businessName
		from user u
			left join business b on u.id=b.accountOwnerId
		where u.id = _userId;
	else
		select INVALID_LOGIN_CREDENTIALS as Result, 'Invalid login credentials.' as ErrorDesc;
	end if;


END$$

DELIMITER ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
