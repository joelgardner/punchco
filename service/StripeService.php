<?php

class StripeService
{
	var $user;
	var $db;
	var $logger;
	function __construct() {
		Stripe::setApiKey(STRIPE_PRIVATE_KEY);
		$this->user = (new AccountService())->getCurrentUser();
		$this->db = new DataAccess();
		$this->logger = new Logger();
	}

	function addCard($token, $card) {
		// Create the charge on Stripe's servers - this will charge the user's card
		try {
			/*
			$charge = Stripe_Charge::create(array(
			  	"amount" => 50, // amount in cents, again
			  	"currency" => "usd",
			  	"card" => $token,
			  	"description" => "joel@grubgrabr.com"
			));
			$this->logger->write('Successfully charged Stripe Token: '.$token);
			*/

			// create a customer
			$customer = Stripe_Customer::create(array(
			  "card" => $token,
			  "description" => $this->user['email']
			));
			$this->logger->write('Stripe: successfully saved Customer: '.$token);

			return $this->db->save_payment_info($this->user['businessId'], $customer->id, $card['type'], $card['last4']);
		} 
		catch(Stripe_CardError $e) {

			$body = $e->getJsonBody();
  			$err  = $body['error'];
  			$this->logger->write('Stripe: create new Customer request failed with message: '.$err['message']);
			return array(
				'Result' => 1,
				'Error' => $err['message']
			);
		}
	}

	function deleteCustomer($stripeCustomerId, $paymentInfoId) {
		try {
			$cu = Stripe_Customer::retrieve($stripeCustomerId);
			$cu->delete();
			$this->logger->write('Deleted stripeCustomerId from Stripe: '.$stripeCustomerId);
		}
		catch (Stripe_InvalidRequestError $ex) {
			$body = $ex->getJsonBody();
  			$err = $body['error'];
  			$this->logger->write('Stripe delete request failed with message (deleting anyway): '.$err['message']);
		}

		return $this->db->delete_paymentinfo($this->user['businessId'], $paymentInfoId);
	}

	function charge($stripeCustomerId, $planId) {
		$prices = array(
			1000,
			1500,
			2000,
			3000
		);
		// Create the charge on Stripe's servers - this will charge the user's card
		try {
			$charge = Stripe_Charge::create(array(
			  	"amount" => $prices[$planId], // amount in cents, again
			  	"currency" => "usd",
			  	"customer" => $stripeCustomerId,
			  	"description" => "FIGURE THIS OUT"
			));
			$this->logger->write('Successfully charged Stripe Customer: '.$stripeCustomerId);
			return array(
				'Result' => 0,
				'charge' => $charge
			);
		} 
		catch(Stripe_CardError $e) {
			// declined
			
			$body = $e->getJsonBody();
  			$err  = $body['error'];
  			$this->logger->write('Stripe charge request failed with message: '.$err['message']);
			return array(
				'Result' => 1,
				'Error' => $err['message']
			);
		}
	}
}


?>
