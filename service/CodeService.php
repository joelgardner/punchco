<?php

class CodeService
{
	var $db;
	var $currentUser;
	function __construct() {
		$this->db = new DataAccess();
		$this->currentUser = (new AccountService())->getCurrentUser();
	} 

	function create($options) {
		// determine how many codes to decrement the available pools by
		$stats = $this->get_code_stats();
		if ($options['ratingsAndReviews'] == 1) {
			$availableSubscriptionCodes = $stats['num_subscription_rr'];
			$availablePermanentCodes = $stats['num_permanent_rr'];
		}
		else {
			$availableSubscriptionCodes = $stats['num_subscription'];
			$availablePermanentCodes = $stats['num_permanent'];
		}

		$quantity = isset($options['quantity']) ? $options['quantity'] : 1;

		// decrement business.num_subscription(_rr) as much as possible before dipping into business.num_permanent(_rr)
		// if we don't have enough subscription OR permanent codes to cover the request, use up the rest of what we ahve
		$num_to_remove_from_subscription_pool = min($availableSubscriptionCodes, $quantity);
		$num_to_remove_from_permanent_pool = min($quantity - $num_to_remove_from_subscription_pool, $availablePermanentCodes);
		$options['num_to_remove_from_subscription_pool'] = $num_to_remove_from_subscription_pool;
		$options['num_to_remove_from_permanent_pool'] = $num_to_remove_from_permanent_pool;
		$options['quantity'] = $num_to_remove_from_subscription_pool + $num_to_remove_from_permanent_pool;
		
		(new Logger())->write('Request to create code batch of size '.$quantity.'\nCodes removed from subscription pool: '.$num_to_remove_from_subscription_pool.'\nCodes removed from permanent pool: '.$num_to_remove_from_permanent_pool);

		if ($options['quantity'] == 0) {
			return array('Result' => 2, 'errorDesc' => 'Insufficient Grubcodes.');		// insufficient amount of available codes
		}

		// check to see if this business has a valid payment method on file
		//$paymentmethods = $this->db->get_payment_info($this->currentUser['businessId']);
		//if (count($paymentmethods) == 0) {
		//	return array('Result' => 3, 'errorDesc' => 'Unable to find a valid payment method.');
		//}

		return $this->db->create_code($this->currentUser["businessId"], json_encode($options));
	}

	function search($options) {
		return $this->db->code_search($this->currentUser["businessId"], json_encode($options));
	}

	function batch($options) {
		return $this->db->create_batch($this->currentUser["businessId"], $options["desc"], $options["num_codes"], $options["num_punches"]);
	}

	function get_code($code) {
		return $this->db->get_code($code);
	}
	function getbatches() {
		return $this->db->get_batches($this->currentUser["businessId"]);
	}

	function redeem($options) { 
		return $this->db->rewardcode_redeem($options["rewardCode"], json_encode($options));
	}

	function get_codes_for_print($options) {
		return $this->db->get_codes_for_print($this->currentUser['businessId'], $options['batchId']);
	}

	function punchcode_redeem($options) {
		$redeemResponse = $this->db->punchcode_redeem($this->currentUser && isset($this->currentUser["id"]) ? $this->currentUser["id"] : null, $options["code"]);
		if ($redeemResponse['Result'] == 0) {
			(new AccountService())->addScoreToCurrentUser($redeemResponse['num_punches']);
		}
		return $redeemResponse;
	}

	function recentactivity($options) {
		return $this->db->code_recentactivity($this->currentUser["id"]);
	}

	function get_code_stats() {
		return $this->db->get_available_code_stats($this->currentUser["businessId"]);
	}

	function submitrating($options) {
		if (isset($options['rating']) && $options['rating'] < 1) {
			$options['rating'] = 1;
		}
		else if (isset($options['rating']) && $options['rating'] > 5) {
			$options['rating'] = 5;
		}

		$submitRatingResponse = $this->db->save_rating($this->currentUser['id'], $options);
		if ($submitRatingResponse['Result'] == 0 && $this->currentUser != null) {
			(new AccountService())->addScoreToCurrentUser(1);
		}
		return $submitRatingResponse;
	}
}

?>