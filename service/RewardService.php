<?php

class RewardService
{
	var $db;
	var $currentUser;
	function __construct() {
		$this->db = new DataAccess();
		$this->currentUser = (new AccountService())->getCurrentUser();
	} 

	function create($options) {
		$accountService = new AccountService();
		$currentUser = $accountService->getCurrentUser();

		//echo "authorid: ".$authorID.'\ncontent:'.$content;
		$reward = $this->db->create_reward($currentUser["businessId"], $currentUser["id"], $options["name"], $options["desc"], $options["req_punches"]);
		return $reward;
	}

	function get() {
		$accountService = new AccountService();
		$currentUser = $accountService->getCurrentUser();
		$rewards = $this->db->get_rewards($currentUser["businessId"]);
		return $rewards;
	}

	function myrewards($options) {
		return $this->db->reward_myrewards($this->currentUser["id"]);
	}

	function userrewards($options) {
		$data = $this->db->reward_userrewards($this->currentUser["id"], $options);
		$businesses = array();
		foreach($data['businesses'] as $business) {
			$business['rewards'] = array();
			$businesses[$business['id'].''] = $business;
		}
		foreach ($data['rewards'] as $reward) {
			array_push($businesses[$reward['businessId'].'']['rewards'], $reward);
		}
		return array_values($businesses);
	}

	function redeem($options) {
		return $this->db->reward_redeem($this->currentUser["id"], $options["rewardId"]);
	}
}

?>
