<?php

class DataAccess 
{
    //public $logger;
    var $db;
    var $logger;
	function __construct() {
		$this->db = new PDO("mysql:host=".DB_SERVER.";dbname=".DB_INITIALDB, DB_USERNAME, DB_PASSWORD);
		$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$this->logger = new Logger();
	}

    function create_code($businessId, $jsonOptions) {
    	$this->logger->write($jsonOptions);
	  	$prep = $this->db->prepare("call code_create(:businessId, :jsonOptions);");
	  	if (!$prep->execute(array(
	  		':businessId' => $businessId,
	  		':jsonOptions' => $jsonOptions))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
	  	}
	  	else {
	  		$row = $prep->fetch(PDO::FETCH_ASSOC);
	  		if (isset($row['batchId']) && $row['batchId']) {
	  			return array(
	  				"Result" => (int)$row["Result"],
	  				"batchId" => (int)$row["batchId"],
	  				"created" => DateTime::createFromFormat("Y-m-d G:i:s", $row["created"])->format('Y-m-d G:i:s'),
	  				"num_punches" => (int)$row["num_punches"],
					"num_codes" => (int)$row["num_codes"],
					"transferable" => (int)$row["transferable"],
					"rr" => (int)$row["ratings_and_reviews"],
					'num_permanent' => (int)$row['num_permanent'],
	  				'num_permanent_rr' => (int)$row['num_permanent_rr'],
	  				'num_subscription' => (int)$row['num_subscription'],
	  				'num_subscription_rr' => (int)$row['num_subscription_rr']
	  			);
	  		}
	  		else {
	  			return array(
	  				"Result" => (int)$row["Result"],
					"codeId" => (int)$row["codeId"],
	  				"code" => $row["code"],
	  				"businessId" => (int)$row["businessId"],
	  				"businessName" => $row["businessName"],
	  				"statusId" => (int)$row["statusId"],
	  				//"statusName" => $row["statusName"],
	  				"num_punches" => (int)$row["num_punches"],
					"rr" => (int)$row["ratings_and_reviews"],
	  				"created" => DateTime::createFromFormat("Y-m-d G:i:s", $row["created"])->format('Y-m-d G:i:s'),
	  				'num_permanent' => (int)$row['num_permanent'],
		  			'num_permanent_rr' => (int)$row['num_permanent_rr'],
		  			'num_subscription' => (int)$row['num_subscription'],
		  			'num_subscription_rr' => (int)$row['num_subscription_rr']
	  			);
	  		}
	  		return $row;
	  	}
    }

    function get_code($code) {
    	$prep = $this->db->prepare("call get_code(:code);");
	  	if (!$prep->execute(array(
	  		':code' => $code))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
	  	}
	  	else {
	  		
	  		$row = $prep->fetch(PDO::FETCH_ASSOC);
	  		if ($row['id'] == 0) {
	  			return null; 
  			}

  			$result = array(
  				"id" => (int)$row["id"],
  				"code" => $row["code"],
  				"businessId" => (int)$row["businessId"],
  				"businessName" => $row["businessName"],
				"typeId" => (int)$row["typeId"],
				"statusId" => (int)$row["statusId"],
				"num_punches" => (int)$row["num_punches"],
				"active" => $row["active"],
				"expire" => $row["expire"],
				"batchId" => (int)$row["batchId"],
				"transferable" => (int)$row["transferable"],
				"rating" => (int)$row["rating"],
				"rr" => (int)$row["ratings_and_reviews"],
				"comment" => $row["comment"],
				"rrCreated" => $row["rrCreated"] ? DateTime::createFromFormat("Y-m-d G:i:s", $row["rrCreated"])->format('Y-m-d G:i:s') : null,
				"created" => $row["created"]
  			);

  			// make sure rating, comment, and rrCreated have values before adding them
  			if ($row["rating"]) {
  				$result["rating"] = (int)$row["rating"];
  			}
  			if ($row["comment"]) {
  				$result["comment"] = $row["comment"];
  			}
  			if ($row["rrCreated"]) {
  				$result["rrCreated"] = DateTime::createFromFormat("Y-m-d G:i:s", $row["rrCreated"])->format('Y-m-d G:i:s');
  			}
  			if ($row["imageKey"]) {
  				$result["imageKey"] = $row["imageKey"];
  			}
	  		
	  		return $result;
	  	}
    }

    function code_search($businessId, $jsonOptions) {
	  	$prep = $this->db->prepare("call code_search(:businessId, :jsonOptions);");
	  	if (!$prep->execute(array(
	  		':businessId' => $businessId,
	  		':jsonOptions' => $jsonOptions))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
	  	}
	  	else {
	  		$results = array();
	  		$i = 0;
	  		while($row = $prep->fetch(PDO::FETCH_ASSOC)) {
	  			$results[$i++] = array(
	  				"id" => (int)$row["id"],
	  				"code" => $row["code"],
	  				"businessId" => (int)$row["businessId"],
					"typeId" => (int)$row["typeId"],
					"statusId" => (int)$row["statusId"],
					"num_punches" => (int)$row["num_punches"],
					"active" => $row["active"],
					"expire" => $row["expire"],
					"batchId" => (int)$row["batchId"],
					"transferable" => (int)$row["transferable"],
					"rating" => (int)$row["rating"],
					"rr" => (int)$row["ratings_and_reviews"],
					"comment" => $row["comment"],
					"rrCreated" => $row["rrCreated"] ? DateTime::createFromFormat("Y-m-d G:i:s", $row["rrCreated"])->format('Y-m-d G:i:s') : null,
					"created" => $row["created"]
	  			);

	  			// make sure rating, comment, and rrCreated have values before adding them
	  			if ($row["rating"]) {
	  				$results[$i - 1]["rating"] = (int)$row["rating"];
	  			}
	  			if ($row["comment"]) {
	  				$results[$i - 1]["comment"] = $row["comment"];
	  			}
	  			if ($row["rrCreated"]) {
	  				$results[$i - 1]["rrCreated"] = DateTime::createFromFormat("Y-m-d G:i:s", $row["rrCreated"])->format('Y-m-d G:i:s');
	  			}
	  		}
	  		return $results;
	  	}
    }


    function get_codes_for_print($businessId, $batchId) {
	  	$prep = $this->db->prepare("call get_codes_for_print(:businessId, :batchId);");
	  	if (!$prep->execute(array(
	  		':businessId' => $businessId,
	  		':batchId' => $batchId))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$results = array();
	  		$i = 0;
	  		while($row = $prep->fetch(PDO::FETCH_ASSOC)) {
	  			$results[$i++] = array(
	  				"id" => (int)$row["id"],
	  				"code" => $row["code"],
	  				"rr" => (int)$row["rr"],
					"t" => (int)$row["transferable"]
	  			);
	  		}
	  		return $results;
	  	}
    }

    function save_codeimage($code, $key) {
	  	$prep = $this->db->prepare("call save_codeimage(:code, :key);");
	  	if (!$prep->execute(array(
	  		':code' => $code,
	  		':key' => $key))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$row = $prep->fetch(PDO::FETCH_ASSOC);
			return array(
  				"Result" => (int)$row["Result"]
  			);
	  	}
    }

    function get_codeimage($code) {
	  	$prep = $this->db->prepare("call get_codeimage(:code);");
	  	if (!$prep->execute(array(
	  		':code' => $code))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$row = $prep->fetch(PDO::FETCH_ASSOC);
	  		if (!$row) {
	  			return null;
	  		}
			return array(
  				"id" => (int)$row["id"],
  				"codeId" => (int)$row["codeId"],
  				"key" => $row["key"],
  				"created" => DateTime::createFromFormat("Y-m-d G:i:s", $row["created"])->format('Y-m-d G:i:s')
  			);
	  	}
    }

    function save_rating($userId, $options) {
    	//$logger = new Logger();
    	//$logger->write('call save_rating('.$userId.', '.$options['code'].', '.$options['rating'].', '.$options['comment'].');');
	  	$prep = $this->db->prepare("call save_rating(:userId, :code, :rating, :comment);");
	  	if (!$prep->execute(array(
	  		':userId' => $userId,
	  		':code' => $options['code'],
	  		':rating' => $options['rating'],
	  		':comment' => isset($options['comment']) ? $options['comment'] : null))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$row = $prep->fetch(PDO::FETCH_ASSOC);
			return array(
  				"Result" => (int)$row["Result"],
  				"ratingsId" => (int)$row["id"],
  				"codeId" => (int)$row["codeId"],
  				"userId" => (int)$row["userId"],
  				"rating" => (int)$row["rating"],
  				"comment" => $row["comment"],
  				"created" => DateTime::createFromFormat("Y-m-d G:i:s", $row["created"])->format('Y-m-d G:i:s')
  			);
	  	}
    }

    function get_feedback($businessId) {
    	$prep = $this->db->prepare("call get_feedback(:businessId);");
	  	if (!$prep->execute(array(
	  		':businessId' => $businessId))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$row = $prep->fetch(PDO::FETCH_ASSOC);
  			$avgRating = (float)$row["avgrating"];

	  		$results = array();
	  		$i = 0;
	  		if ($prep->nextRowset()) {
		  		while($row = $prep->fetch(PDO::FETCH_ASSOC)) {
		  			$results[$i++] = array(
		  				"id" => (int)$row["id"],
		  				"codeId" => (int)$row["codeId"],
		  				"code" => $row["code"],
		  				"userId" => (int)$row["userId"],
		  				"username" => $row["name"],
		  				"rating" => (int)$row["rating"],
						"comment" => $row["comment"],
						"created" => DateTime::createFromFormat("Y-m-d G:i:s", $row["created"])->format('Y-m-d G:i:s'),
						"userScore" => (int)$row["userScore"]
		  			);
		  			if ($row["imageKey"]) {
		  				$results[$i - 1]["imageKey"] = $row["imageKey"];
		  			}
		  		}
	  		}
	  		return array('avgrating' => $avgRating, 'feedback' => $results);
	  	}
    }

    function get_feedback_for_code($code) {
		$prep = $this->db->prepare("call get_feedback_for_code(:code);");
	  	if (!$prep->execute(array(
	  		':code' => $code))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
	  	}
	  	else {
	  		
	  		$row = $prep->fetch(PDO::FETCH_ASSOC);
  			$result = array(
				"businessName" => $row["businessName"],
  			);

  			// make sure rating, comment, and rrCreated have values before adding them
  			if ($row["rating"]) {
  				$result["rating"] = (int)$row["rating"];
  			}
  			if ($row["comment"]) {
  				$result["comment"] = $row["comment"];
  			}
  			if ($row["rrCreated"]) {
  				$result["rrCreated"] = DateTime::createFromFormat("Y-m-d G:i:s", $row["rrCreated"])->format('Y-m-d G:i:s');
  			}
  			if ($row["imageKey"]) {
  				$result["imageKey"] = $row["imageKey"];
  			}

  			if (isset($row["reviewer"])) {
  				$result["reviewer"] = $row["reviewer"];
  			}
	  		
	  		return $result;
	  	}
    }

    function rewardcode_redeem($rewardCode, $jsonOptions) {
	  	$prep = $this->db->prepare("call rewardcode_redeem(:rewardCode, :jsonOptions);");
	  	if (!$prep->execute(array(
	  		':rewardCode' => $rewardCode,
	  		':jsonOptions' => $jsonOptions))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$results = array();
	  		$i = 0;
	  		while($row = $prep->fetch(PDO::FETCH_ASSOC)) {
	  			$results[$i++] = $row;
	  		}
	  		return $results;
	  	}
    }

    function punchcode_redeem($userId, $code) {
    	$logger = new Logger();
    	$logger->write("call punchcode_redeem($code, $userId);");
	  	$prep = $this->db->prepare("call punchcode_redeem(:code, :userId);");
	  	if (!$prep->execute(array(
	  		':code' => $code,
	  		':userId' => $userId))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$row = $prep->fetch(PDO::FETCH_ASSOC);
	  		return $row;
	  	}
    }

    function code_recentactivity($userId) {
	  	$prep = $this->db->prepare("call code_recentactivity(:userId);");
	  	if (!$prep->execute(array(
	  		':userId' => $userId))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$results = array();
	  		$i = 0;
	  		while($row = $prep->fetch(PDO::FETCH_ASSOC)) {
	  			$results[$i++] = array(
	  				"codeId" => (int)$row["codeId"],
	  				"code" => $row["code"],
	  				"businessId" => (int)$row["businessId"],
	  				"businessName" => $row["BusinessName"],
	  				"statusId" => (int)$row["statusId"],
	  				"statusName" => $row["statusName"],
	  				"num_punches" => (int)$row["num_punches"],
	  				"punched" => DateTime::createFromFormat("Y-m-d G:i:s", $row["punched"])->format('Y-m-d G:i:s'),
	  				"rr" => (int)$row["ratings_and_reviews"]
	  			);

	  			// make sure rating, comment, and rrCreated have values before adding them
	  			if ($row["rating"]) {
	  				$results[$i - 1]["rating"] = (int)$row["rating"];
	  			}
	  			if ($row["comment"]) {
	  				$results[$i - 1]["comment"] = $row["comment"];
	  			}
	  			if ($row["rrCreated"]) {
	  				$results[$i - 1]["rrCreated"] = DateTime::createFromFormat("Y-m-d G:i:s", $row["rrCreated"])->format('Y-m-d G:i:s');
	  			}
	  			if ($row["imageKey"]) {
	  				$results[$i - 1]["imageKey"] = $row["imageKey"];
	  			}
	  		}
	  		return $results;
	  	}
    }

    function reward_userrewards($userId, $options) {
	  	$prep = $this->db->prepare("call get_user_rewards(:userId);");
	  	if (!$prep->execute(array(
	  		':userId' => $userId))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$resultsA = array();
	  		$i = 0;
	  		while($row = $prep->fetch(PDO::FETCH_ASSOC)) {
	  			$resultsA[$i++] = array(
	  				"id" => (int)$row["businessId"],
	  				"businessName" => $row["businessName"],
	  				"score" => (int)$row["businessScore"],
	  				"availPunches" => (int)$row["availablePunches"]
	  			);
	  		}
	  		$resultsB = array();
	  		$i = 0;
	  		if ($prep->nextRowset()) {
		  		while($row = $prep->fetch(PDO::FETCH_ASSOC)) {
		  			$resultsB[$i++] = array(
		  				"rewardId" => (int)$row["rewardId"],
		  				"businessId" => (int)$row["businessId"],
		  				"rewardName" => $row["rewardName"],
		  				"rewardDesc" => $row["rewardDesc"],
		  				"required_punches" => (int)$row["required_punches"]
		  			);
		  		}
	  		}
	  		return array('businesses' => $resultsA, 'rewards' => $resultsB);
	  	}
    }

    function reward_myrewards($userId) {
	  	$prep = $this->db->prepare("call reward_myrewards(:userId);");
	  	if (!$prep->execute(array(
	  		':userId' => $userId))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$results = array();
	  		$i = 0;
	  		while($row = $prep->fetch(PDO::FETCH_ASSOC)) {
	  			$results[$i++] = array(
	  				"rewardCodeId" => (int)$row["rewardCodeId"],
	  				"businessId" => (int)$row["businessId"],
	  				"businessName" => $row["businessName"],
	  				"code" => $row["code"],
	  				"rewardId" => (int)$row["rewardId"],
	  				"rewardName" => $row["rewardName"],
	  				"rewardDesc" => $row["rewardDesc"],
	  				"statusId" => (int)$row["statusId"],
	  				"required_punches" => (int)$row["required_punches"],
	  				"created" => DateTime::createFromFormat("Y-m-d G:i:s", $row["created"])->format('Y-m-d G:i:s')
	  			);
	  		}
	  		return $results;
	  	}
    }

    function reward_redeem($userId, $rewardId) {
	  	$prep = $this->db->prepare("call reward_redeem(:userId, :rewardId);");
	  	if (!$prep->execute(array(
	  		':userId' => $userId,
	  		':rewardId' => $rewardId))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$row = $prep->fetch(PDO::FETCH_ASSOC);
	  		if ($row["Result"] != 0) {
	  			return $row;
	  		}

  			$resultsA = array(
  				"Result" => (int)$row["Result"],
				"businessId" => (int)$row["businessId"],
  				"businessName" => $row["businessName"],
  				"rewardId" => (int)$row["rewardId"],
  				"rewardName" => $row["rewardName"],
  				"rewardDesc" => $row["rewardDesc"],
  				"code" => $row["code"],
  				"required_punches" => (int)$row["required_punches"],
  				"statusId" => (int)$row["statusId"],
  				"rewardCodeId" => (int)$row["rewardCodeId"],
  				"statusName" => $row["statusName"],
  				"businessId" => (int)$row["businessId"],
  				"created" => DateTime::createFromFormat("Y-m-d G:i:s", $row["created"])->format('Y-m-d G:i:s')
  			);
	  		
	  		$resultsB = array();
	  		$i = 0;
	  		if ($prep->nextRowset()) {
		  		while($row = $prep->fetch(PDO::FETCH_ASSOC)) {
		  			$resultsB[$i++] = array(
		  				"id" => (int)$row["id"],
						"businessId" => (int)$row["businessId"],
		  				"rewardId" => (int)$row["rewardId"],
		  				"code" => $row["code"],
		  				"num_punches" => (int)$row["num_punches"],
		  				"statusId" => (int)$row["statusId"],
		  				"created" => DateTime::createFromFormat("Y-m-d G:i:s", $row["created"])->format('Y-m-d G:i:s'),
		  				"punched" => DateTime::createFromFormat("Y-m-d G:i:s", $row["punched"])->format('Y-m-d G:i:s')
		  			);
		  		}
	  		}
	  		return array('reward' => $resultsA, 'punchedCodes' => $resultsB);
	  	}
    }


    function get_codes($businessId, $jsonOptions) {
	  	$prep = $this->db->prepare("call get_codes(:businessId);");
	  	if (!$prep->execute(array(':businessId' => $businessId))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$results = array();
	  		$i = 0;
	  		while($row = $prep->fetch(PDO::FETCH_ASSOC)) {
	  			$results[$i++] = array(
	  				"batchId" => (int)$row["batchId"],
	  				"created" => DateTime::createFromFormat("Y-m-d G:i:s", $row["created"])->format('Y-m-d G:i:s'),
	  				"num_punches" => (int)$row["num_punches"],
					"num_codes" => (int)$row["num_codes"],
					"transferable" => $row["transferable"],
					"rr" => (int)$row["ratings_and_reviews"]
	  			);
	  		}
	  		return $results;
	  	}
    }

    function get_available_code_stats($businessId) {
   	  	$prep = $this->db->prepare("call get_available_code_stats(:businessId);");
	  	if (!$prep->execute(array(':businessId' => $businessId))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$row = $prep->fetch(PDO::FETCH_ASSOC);
	  		return array(
	  			'num_permanent' => (int)$row['num_permanent'],
	  			'num_permanent_rr' => (int)$row['num_permanent_rr'],
	  			'num_subscription' => (int)$row['num_subscription'],
	  			'num_subscription_rr' => (int)$row['num_subscription_rr']
	  		);
	  	}
    }

    function create_reward($businessId, $ownerId, $name, $desc, $num_punches) {
	  	$prep = $this->db->prepare("call addoredit_reward(:id, :businessId, :ownerId, :name, :desc, :num_punches);");
	  	if (!$prep->execute(array(
	  		':id' => NULL,
	  		':businessId' => $businessId,
	  		':ownerId' => $ownerId,
	  		':name' => $name,
	  		':desc' => $desc,
	  		':num_punches' => $num_punches))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
	  	}
	  	else {
	  		$row = $prep->fetch(PDO::FETCH_ASSOC);
	  		return $row;
	  	}
    }

    /// ACCOUNTS
	function add_user($email, $username, $password, $businessName, $firstName, $lastName) {
		$passwordHash = $this->generate_hashed_password($password);
	  	$prep = $this->db->prepare("call add_user(:email, :username, :passwordHash, :businessName, :firstName, :lastName);");
	  	if (!$prep->execute(array(
	  		':email' => strlen($email) ? $email : null,
	  		':username' => $username,
	  		':passwordHash' => $passwordHash,
	  		':businessName' => strlen($businessName) ? $businessName : null,
	  		':firstName' => $firstName,
	  		':lastName' => $lastName))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$row = $prep->fetch(PDO::FETCH_ASSOC);
	  		if ($row["Result"] == 0) {
	  			$result = array(
	  				"Result" => $row["Result"],
	  				"id" => (int)$row["id"],
	  				"email" => $row["email"],
	  				"name" => $row["name"],
					"firstName" => $row["firstName"],
					"lastName" => $row["lastName"],
					"score" => (int)$row["score"],
					"registerDate" => DateTime::createFromFormat("Y-m-d G:i:s", $row["registerDate"])->format('Y-m-d G:i:s'),
					//"lastLoginDate" => DateTime::createFromFormat("Y-m-d G:i:s", $row["lastLoginDate"])->format('Y-m-d G:i:s'),
					"lastChangeDate" => DateTime::createFromFormat("Y-m-d G:i:s", $row["lastChangeDate"])->format('Y-m-d G:i:s'),
					//"businessId" => (int)$row["businessId"],
					//"ownerId" => (int)$row["ownerId"],
					//"subdomain" => $row["subdomain"]
	  			);

	  			if ($row["signupKey"]) {
	  				$result["signupKey"] = $row["signupKey"];
	  			}
	  		}
	  		else if ($row["Result"] > 0) {
	  			$result = array(
		  			"Result" => $row["Result"],
	  				"errorDesc" => $row["ErrorDesc"]
	  			);
	  		}

	  		return $result;
	  	}
	}

	function verify_signup_key($signupKey) {
		$prep = $this->db->prepare("call verify_signup_key(:signupKey);");
	  	if (!$prep->execute(array(
	  		':signupKey' => $signupKey))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$row = $prep->fetch(PDO::FETCH_ASSOC);
	  		return $row;
	  	}
	}

	function save_payment_info($businessId, $stripeCustomerId, $cardType, $last4digits) {
	  	$prep = $this->db->prepare("call save_payment_info(:businessId, :stripeCustomerId, :cardType, :last4digits);");
	  	if (!$prep->execute(array(
	  		':businessId' => $businessId,
	  		':stripeCustomerId' => $stripeCustomerId,
	  		':cardType' => $cardType,
	  		':last4digits' => $last4digits))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$row = $prep->fetch(PDO::FETCH_ASSOC);
	  		return $row;
	  	}
	}

	function save_business_transaction($businessId, $paymentInfoId, $ratingsAndReviews, $numCodes, $subscription, $chargeAmount) {
	  	$prep = $this->db->prepare("call save_business_transaction(:businessId, :paymentInfoId, :ratingsAndReviews, :numCodes, :subscription, :chargeAmount);");
	  	if (!$prep->execute(array(
	  		':businessId' => $businessId,
	  		':paymentInfoId' => $paymentInfoId,
	  		':ratingsAndReviews' => $ratingsAndReviews,
	  		':numCodes' => $numCodes,
	  		':subscription' => $subscription,
	  		':chargeAmount' => $chargeAmount))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$row = $prep->fetch(PDO::FETCH_ASSOC);
	  		return array(
	  			'Result' => (int)$row['Result'],
	  			'num_permanent' => (int)$row['num_permanent'],
	  			'num_permanent_rr' => (int)$row['num_permanent_rr'],
	  			'num_subscription' => (int)$row['num_subscription'],
	  			'num_subscription_rr' => (int)$row['num_subscription_rr']
	  		);
	  		return $row;
	  	}
	}

	function get_payment_info($businessId) {
	  	$prep = $this->db->prepare("call get_payment_info(:businessId);");
	  	if (!$prep->execute(array(
	  		':businessId' => $businessId))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$results = array();
	  		$i = 0;
	  		while($row = $prep->fetch(PDO::FETCH_ASSOC)) {
	  			$results[$i++] = array(
	  				"id" => (int)$row["id"],
	  				"cardType" => $row["cardType"],
					"last4" => $row["last4"]
	  			);
	  		}
	  		return $results;
	  	}
	}

	function get_paymentinfo_customerId($businessId, $paymentInfoId) {
		$prep = $this->db->prepare("call get_paymentinfo_customerId(:businessId, :paymentInfoId);");
	  	if (!$prep->execute(array(
	  		':businessId' => $businessId,
	  		':paymentInfoId' => $paymentInfoId))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		if ($row = $prep->fetch(PDO::FETCH_ASSOC)) {
	  			return $row["stripeCustomerId"];
	  		}
	  		return null;
	  	}
	}

	function increase_permanent_codes($businessId, $num) {
		$prep = $this->db->prepare("call increase_permanent_codes(:businessId, :num);");
	  	if (!$prep->execute(array(
	  		':businessId' => $businessId,
	  		':num' => $num))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$row = $prep->fetch(PDO::FETCH_ASSOC);
	  		return $row;
	  	}
	}

	function delete_paymentinfo($businessId, $paymentInfoId) {
		//$this->logger->write("call delete_paymentinfo(".$businessId.", ".$paymentInfoId.");");
		$prep = $this->db->prepare("call delete_paymentinfo(:businessId, :paymentInfoId);");
	  	if (!$prep->execute(array(
	  		':businessId' => $businessId,
	  		':paymentInfoId' => $paymentInfoId))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$row = $prep->fetch(PDO::FETCH_ASSOC);
	  		return $row;
	  	}
	}

	function get_rewards($businessId) {
	  	$prep = $this->db->prepare("call get_rewards(:businessId);");
	  	if (!$prep->execute(array(
	  		':businessId' => $businessId))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$results = array();
	  		$i = 0;
	  		while($row = $prep->fetch(PDO::FETCH_ASSOC)) {
	  			$results[$i++] = array(
	  				"id" => (int)$row["id"],
	  				"businessId" => (int)$row["businessId"],
	  				"creatorId" => (int)$row["creatorId"],
					"name" => $row["name"],
					"desc" => $row["desc"],
					"required_punches" => (int)$row["required_punches"],
					"num_redeemed" => (int)$row["num_redeemed"],
					"created" => DateTime::createFromFormat("Y-m-d G:i:s", $row["created"])->format('Y-m-d G:i:s'),
					"changed" => DateTime::createFromFormat("Y-m-d G:i:s", $row["changed"])->format('Y-m-d G:i:s')
	  			);
	  		}
	  		return $results;
	  	}
	}

	function get_batches($businessId) {
		return $this->get_codes($businessId, null);
	}

	function create_batch($businessId, $desc, $num_codes, $num_punches) {
	  	$prep = $this->db->prepare("call create_batch(:businessId, :desc, :num_codes, :num_punches);");
	  	if (!$prep->execute(array(
	  		':businessId' => $businessId,
	  		':desc' => $desc,
	  		':num_codes' => $num_codes,
	  		':num_punches' => $num_punches))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$row = $prep->fetch(PDO::FETCH_ASSOC);
	  		return $row;
	  	}
	}

	function user_login($username, $password) {
		$salt = $this->get_salt_by_username($username);
		if ($salt == null) {
			return array(
		  			"result" => 1,
	  				"errorDesc" => "Invalid login credentials."
	  			);;
		}

		$passwordHash = crypt($password, $salt);
	  	$prep = $this->db->prepare("call user_login(:username, :passwordHash);");
	  	if (!$prep->execute(array(
	  		':username' => $username,
	  		':passwordHash' => $passwordHash))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$row = $prep->fetch(PDO::FETCH_ASSOC);
	  		if ($row["Result"] == 0) {
		  		return array(
	  				"id" => (int)$row["id"],
	  				"email" => $row["email"],
	  				"name" => $row["name"],
					"firstName" => $row["firstName"],
					"lastName" => $row["lastName"],
					"score" => (int)$row["score"],
					"registerDate" => DateTime::createFromFormat("Y-m-d G:i:s", $row["registerDate"])->format('Y-m-d G:i:s'),
					"lastLoginDate" => DateTime::createFromFormat("Y-m-d G:i:s", $row["lastLoginDate"])->format('Y-m-d G:i:s'),
					//"lastChangeDate" => DateTime::createFromFormat("Y-m-d G:i:s", $row["lastChangeDate"])->format('Y-m-d G:i:s'),
					"businessId" => (int)$row["businessId"],
					"businessName" => $row["businessName"]
	  			);
	  		}
	  		return array(
	  			"Result" => $row["Result"],
  				"errorDesc" => $row["ErrorDesc"]
  			);
	  	}
	}

	function get_salt_by_username($username) {
		$prep = $this->db->prepare("call get_salt_by_username(:username);");
	  	if (!$prep->execute(array(
	  		':username' => $username))) {
	  		$error = $prep->errorInfo();
   			$this->logger->write("MySQL Error: {$error[2]}");
   			return false;
	  	}
	  	else {
	  		$row = $prep->fetch(PDO::FETCH_ASSOC);
	  		return $row['salt'];
	  	}
	}

	// these next two functions should probably be somewhere else
	function generate_hashed_password($password) {
		if (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {
		 	$salt = $this->generate_random_salt();
			return crypt($password, $salt);
		}
		return null;
	}
	function generate_random_salt() {
		return '$2y$11$'.substr(md5(uniqid(rand(), true)), 0, 22);
	}
}

?>