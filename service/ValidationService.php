<?php

class ValidationService
{
	function validate($service, $action, $params) {
		$patterns = array(
			"account" => array(
				"login" => array(
					"username" => "/^[ ._a-zA-Z0-9]+$/"
				),
				"create" => array(
					"businessName" => "/^[!& \'a-zA-Z0-9]+$/",
					"firstName" => "/^[ \'a-zA-Z0-9]+$/",
        			"lastName" => "/^[ \'a-zA-Z0-9]+$/",
        			"email" => "/[a-zA-Z0-9._+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9]{2,4}/",
        			"username" => "/^[ ._a-zA-Z0-9]+$/"
				)
			),
			"code" => array(
				"punchcode_redeem" => array(
					"code" => "/^[ a-zA-Z0-9]+$/"
				)
			),
			"reward" => array(
				"create" => array(
					"desc" => "/^[ :;'\"\{\}|\[\]\/,+=\?*%$#@!`~()\-&._a-zA-Z0-9]+$/",
					"name" => "/^[ :;'\"\{\}|\[\]\/,+=\?*%$#@!`~()\-&._a-zA-Z0-9]+$/",
					"req_punches" => "/^\d{1,3}$/"
				)
			)
		);

		if (!$params || !isset($params['options'])) {
			return true;
		}

		foreach($params['options'] as $input => $value) {
			if (isset($patterns[$service]) && 
				isset($patterns[$service][$action]) && 
				isset($patterns[$service][$action][$input]) && 
				!preg_match($patterns[$service][$action][$input], $value)) {
					(new Logger())->write("validation failed: ".$input.' => '.$value);
					return false;
			}
		}
		
		return true;
	}
}

?>