<?php

class FeedbackService
{
	var $db;
	var $currentUser;
	function __construct() {
		$this->db = new DataAccess();
		$this->currentUser = (new AccountService())->getCurrentUser();
	} 


	function get() {
		$accountService = new AccountService();
		$currentUser = $accountService->getCurrentUser();
		return $this->db->get_feedback($currentUser["businessId"]);
	}

	function get_feedback_for_code($code) {
		return $this->db->get_feedback_for_code($code);
	}
}

?>
