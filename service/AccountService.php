<?php

class AccountService
{
	var $db;
	var $logger;
	function __construct() {
		$this->db = new DataAccess();
		$this->logger = new Logger();
	} 

	function login($options) {
		$user = $this->db->user_login($options['username'], $options['password']);
		if ($user && isset($user['id']) && $user['id']) {
			unset($_SESSION['user']);
			$_SESSION['user'] = $user;
			$results = array();
			$results['user'] = $user;

			if (isset($options['code']) && $options['code']) {
				$codeSvc = new CodeService();
				$code = $codeSvc->punchcode_redeem(array('code' => $options['code']));
				$results['user']['score'] += $code['num_punches'];
			}

			// these calls hsould be via a service, not directly to data layer
			$results['mycodes'] = $this->db->code_recentactivity($user['id']);
			$results['myrewards'] = $this->db->reward_myrewards($user['id']);
			$results['userrewards'] = (new RewardService())->userrewards(null);
			$results['Result'] = '0';
			return $results;
		}
		else {
			unset($_SESSION['user']);
			session_destroy();


			return $user;
		}
	}

	function apitoken($options) {
		$user = $this->db->user_login($options['username'], $options['password']);
		if ($user && isset($user['id']) && $user['id']) {
			unset($_SESSION['user']);
			//$_SESSION['user'] = $user;
			$results = array();
			$results['user'] = $user;

			//$apiKey = $this->db->set_api_key$(get_random_api_key(), $user['id']);
			$results['apiKey'] = $apiKey;
			//if (isset($options['code']) && $options['code']) {
			//	$codeSvc = new CodeService();
			//	$code = $codeSvc->punchcode_redeem(array('code' => $options['code']));
			//	$results['user']['score'] += $code['num_punches'];
			//}

			// these calls hsould be via a service, not directly to data layer
			//$results['mycodes'] = $this->db->code_recentactivity($user['id']);
			//$results['myrewards'] = $this->db->reward_myrewards($user['id']);
			//$results['userrewards'] = (new RewardService())->userrewards(null);
			//$results['Result'] = '0';
			return $results;
		}
		else {
			unset($_SESSION['user']);
			session_destroy();


			return $user;
		}
	}

	function verifySignupKey($signupKey) {
		$result = $this->db->verify_signup_key($signupKey);
		if ($result['Result'] != 0) {

		}

		return $result;
	}

	function create($options) {
		$user = $this->db->add_user(
			isset($options['email']) ? $options['email'] : null, 
			$options['username'], 
			$options['password'], 
			isset($options['businessName']) ? $options['businessName'] : null,
			isset($options['firstName']) ? $options['firstName'] : null,
			isset($options['lastName']) ? $options['lastName'] : null
			//isset($options['subdomain']) ? $options['subdomain'] : null
		);

		if (!$user || $user["Result"] > 0) {
			return $user;
		}

		// send the user a
		if (isset($user['signupKey'])) {
			$emailService = new EmailService();
			$emailService->SendIntroduction($user);
			unset($user['signupKey']);	// we don't want to put this in the response
		}
		return $this->login($options);
	}

	function logout() {
		unset($_SESSION['user']);
		session_destroy();
	}

	function current() {
		return $this->getCurrentUser();
	}

	function getCurrentUser() {
		return isset($_SESSION['user']) ? $_SESSION['user'] : null;
	}

	function get_payment_info() {
		return $this->db->get_payment_info($this->getCurrentUser()['businessId']);
	}

	function delete_paymentinfo($options) {
		$stripeCustomerId = $this->db->get_paymentinfo_customerId($this->getCurrentUser()['businessId'], $options['id']);
		return (new StripeService())->deleteCustomer($stripeCustomerId, $options['id']);
	}

	function submitPayment($options) {
		$user = $this->getCurrentUser();
		$planId = $options['planId'];
		$paymentInfoId = $options['paymentInfoId'];

		$stripeCustomerId = $this->db->get_paymentinfo_customerId($user['businessId'], $paymentInfoId);
		if (!$stripeCustomerId) {
			return;	// could really only occur via malicious vector, so return nothing
		}
		$stripeService = new StripeService();
		$stripeCharge = $stripeService->charge($stripeCustomerId, $planId);
		if ($stripeCharge['Result'] == 0) {
			// successfully charged stripe card, so now we increase available codes
			$num_codes_to_add = array(
				2000,
				2000,
				5000,
				5000
			);

			$prices = array(
				10,
				15,
				20,
				30
			);

			// if it is a one-time purchase, the permanent code count will increase
			if ($planId == 0 || $planId == 1) {
				return $this->db->save_business_transaction(
					$user['businessId'], 
					$paymentInfoId, 
					$planId == 0 ? 0 : 1, 	// ratings and reviews
					$num_codes_to_add[$planId], 
					0, 	// subscription
					$prices[$planId]
				);
			}
			// if a subscription, the subscription count will increase (number of subscription codes do not roll over month to month)
			else if ($planId == 2 || $planId == 3) {
				return $this->db->save_business_transaction(
					$user['businessId'], 
					$paymentInfoId, 
					$planId == 2 ? 0 : 1, 	// ratings and reviews
					$num_codes_to_add[$planId], 
					1, 	// subscription
					$prices[$planId]
				);
			}

			// TODO: is user purchasing a subscription?
		}
		else {
			return $stripeCharge;
		}
	}

	function addcard($options) {
		$user = $this->getCurrentUser();

		$stripeService = new StripeService();
		return $stripeService->addCard($options['stripeToken'], $options['card']);
	}

	function addScoreToCurrentUser ($score) {
		$_SESSION['user']['score'] = $_SESSION['user']['score'] + (int)$score;
	}

	// TODO: put this logic into a service... GateKeeperService?
	function currentUserIsAllowedToPerformAction($service, $action) {
		$user = $this->getCurrentUser();

		if (!$user) {	// anonymous users
			$allowAnonymouseUserActions = array(
				'code' => array(
					'punchcode_redeem',
					'submitrating'
				),
				'account' => array(
					'login',
					'create',
					'logout'
				)
			);
			if (in_array($action, isset($allowAnonymouseUserActions[$service]) ? $allowAnonymouseUserActions[$service] : []))  {
				return true;
			}
			$ipAddress = 'get the ip address';
			$this->logger->write('Unauthorized attempt by anonymouse user ('.$ipAddress.') to access service:/'.$service.'/'.$action.'!');
		}
		else if ($user && !$user['businessId']) {		// logged in users who are NOT business account owners
			$allowUserActions = array(
				'code' => array(
					'punchcode_redeem',
					'submitrating'
				),
				'account' => array(
					'login',
					'create',
					'logout'
				),
				'reward' => array(
					'redeem'
				)
			);
			if (in_array($action, isset($allowUserActions[$service]) ? $allowUserActions[$service] : []))  {
				return true;
			}

			$ipAddress = 'get the ip address';
			$this->logger->write('Unauthorized attempt by regular user '.$user['name'].' ('.$user['id'].') to access service /'.$service.'/'.$action.'!');
		}
		else {		// logged in users who are business account owners, who can do everything
			return true;
		}

		
		return false;
	}
}

?>