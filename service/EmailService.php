<?php

class EmailService
{
	var $logger;
	var $db;
	function __construct() {
		$this->logger = new Logger();
		$this->db = new DataAccess();
	} 

	function SendIntroduction($user) {
		$mail = new PHPMailer;

		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = EMAIL_SMTP_SERVER;  // Specify main and backup server
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = EMAIL_USERNAME;                            // SMTP username
		$mail->Password = EMAIL_PASSWORD;                           // SMTP password
		$mail->SMTPSecure = 'ssl'; 
		$mail->Port = 465;

		$mail->From = 'joel@grubgrabr.com';
		$mail->FromName = 'Grubgrabr';
		$mail->addAddress($user['email'], $user['firstName'].' '.$user['lastName']);  // Add a recipient

		$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
		//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
		//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
		$mail->isHTML(true);                                  // Set email format to HTML

		$mail->Subject = "Welcome!";
		$mail->Body    = "<p>Thank you for signing up. It is my goal to provide a low-price, high-value product that is easy to use for the restaurant and customer alike.  So, I invite you to email me with any questions, recommendations, bugs, complaints, etc. you may have about Grubgrabr.</p><p>To get started, activate your account at:</p><a href=\"http://grubgrabr.com/account/activate/".$user['signupKey']."\">grubgrabr.com/account/activate/".$user['signupKey']."</a><br /><p>Upon activation, <strong>300 free Ratings & Reviews enabled Grubcodes</strong> will be added to your account.  Sign in by clicking Login at the top right corner.  Navigate to the Codes > Generate tab and create your Grubcodes.  From there you can print them or allow customers to scan QR-codes, etc.</p><br />Thanks,<br />Joel at Grubgrabr";
		$mail->AltBody = "Thank you for signing up. It is my goal to provide a low-price, high-value product that is easy to use for the restaurant and customer alike.  So, I invite you to email me with any questions, recommendations, bugs, complaints, etc. you may have about Grubgrabr.  To get started, activate your account at: http://grubgrabr.com/account/activate/".$user['signupKey']."  Upon activation, 300 free Ratings & Reviews enabled Grubcodes will be added to your account.  Sign in by clicking Login at the top right corner.  Navigate to the Codes > Generate tab and create your Grubcodes.  From there you can print them or allow customers to scan their QR-code, etc.  Thanks.  Joel at Grubgrabr";

		if(!$mail->send()) {
		   $this->logger->write('Email could not be sent.  Mailer Error: ' . $mail->ErrorInfo);
		   return;
		}

	}
}

?>
