<?php
use Aws\S3\S3Client;

class ImageService
{
	var $logger;
	var $db;
	function __construct() {
		$this->logger = new Logger();
		$this->db = new DataAccess();
	} 

	function SaveUploadedImage($tmp_path, $file_name, $code) {
		$this->logger->write('Writing to S3 for '.$code.' // path: '.$tmp_path.' // fileName: '.$file_name);
		
		// do some verifying of the filename
		$fileNameParts = explode(".", $file_name);
		$fileExt = strtolower($fileNameParts[1]);
		if (count($fileNameParts) != 2 || 
			!(	$fileExt == 'png' ||
				$fileExt == 'jpg' ||
				$fileExt == 'jpeg'
			)) {
			$this->logger('rejecting file '.$file_name);
			exit;
		}

		// create key which is just hte filename
		$key = $code.'.'.$fileExt;

		// create a thumbnail
		$image = new Imagick($tmp_path);
		$image->thumbnailImage(100, 100);
		$this->correctOrientation($image);
		$image->writeImage('../dev/images/thumbnails/'.$key);
		$image->destroy();

		// create a (compressed) large version
		$image = new Imagick($tmp_path);
		$image->scaleImage(800, 800, true);
		$image->writeImage('../dev/images/'.$key);
		$image->destroy();

		// get current image info for this code
		$currentCodeImage = $this->db->get_codeimage($code);
		if ($currentCodeImage) {
			// TODO: delete current image from S3
		}

		// get metadata uploadedby
		$user = (new AccountService())->getCurrentUser();
		$userDesc = 'Anonymous';
		if ($user) {
			$userDesc = $user['name'];
		}

		// create S3 client
		$client = S3Client::factory(array(
		    'key'    => S3_ACCESS_KEY_ID,
		    'secret' => S3_SECRET_ACCESS_KEY
		));

		// upload image to S3 (the thumbnail and the large version)
		foreach (array(
			$key => '../dev/images/'.$key,
			'thumb/'.$key => '../dev/images/thumbnails/'.$key
		) as $fileKey => $filePath) {

			$result = $client->putObject(array(
			    'Bucket'     => S3_BUCKET,
			    'Key'        => $fileKey,
			    'SourceFile' => $filePath,
			    'ACL'        => 'public-read',
			    'Metadata'   => array(
			        'Code' => $code,
			        'UploadedFileName' => $file_name,
			        'UploadedBy' => $userDesc
			    )
			));

			unlink($filePath);
		}

		// save a reference to this in our db
		$saveResponse = $this->db->save_codeimage($code, $key);

		// Access parts of the result object
		$this->logger->write('Successfully uploaded file '.$key.' to S3.');

		echo json_encode(array('key' => $key));
	}

	function correctOrientation($image) {
		// correct the orientation of the thumbnail image
	    switch($image->getImageOrientation()) {
	        case imagick::ORIENTATION_BOTTOMRIGHT: 
	            $image->rotateimage("#000", 180); // rotate 180 degrees
	        	break;
	        case imagick::ORIENTATION_RIGHTTOP:
	            $image->rotateimage("#000", 90); // rotate 90 degrees CW
	        	break;
	        case imagick::ORIENTATION_LEFTBOTTOM: 
	            $image->rotateimage("#000", -90); // rotate 90 degrees CCW
	        	break;
	    }
	    // Now that it's auto-rotated, make sure the EXIF data is correct in case the EXIF gets saved with the image!
	    $image->setImageOrientation(imagick::ORIENTATION_TOPLEFT);
	}
}

?>
