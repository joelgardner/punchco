<?php

class CodesController extends BaseController
{
	function Index() {
		$accountService = new AccountService();
		$user = $accountService->getCurrentUser();
		$batches = $this->db->get_batches($user["businessId"]);

		$codes = $this->db->get_codes($user["businessId"], null);
		$code_stats = $this->db->get_available_code_stats($user["businessId"]);
		$model = array(
			"batches" => $codes,
			"code_stats" => $code_stats
		);
		$this->Render($model);
	}

	function Create() {
		$model = array(

		);
		$this->Render($model);
	}
}

?>