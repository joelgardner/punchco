<?php

class ProfileController extends BaseController
{
	function __construct($request) {
		parent::__construct($request);
	}
	function Index($username) {
		$profile = $this->db->get_profile($username);
		$posts = $this->db->get_posts($profile["id"]);

		$model = array(
			'posts' => $posts,
			'profile' => $profile
		);
		
		$this->Render($model);
	}
}

?>