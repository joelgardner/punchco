<?php

class BaseController
{
	public $Request;
	var $subdomain;
	var $db;
	var $allowAnonymous;
	var $logger;
	var $validationService;
	var $optimizeForSearchBot;

	function __construct($request) {
		$this->Request = $request;
		$this->db = new DataAccess();
		$this->allowAnonymous = false;
		$this->logger = new Logger();
		$this->validationService = new ValidationService();

		// parse the brand from the url, if any
		$serverName = $_SERVER["SERVER_NAME"];
		if (strpos($serverName, "punchco") > 0) {
			$this->subdomain = substr($serverName, 0, strpos($serverName, '.'));
		}

		// is this request from the googlebot (or the like)?
		if (strstr(strtolower($_SERVER['HTTP_USER_AGENT']), "googlebot")) {
			$this->optimizeForSearchBot = true;
		}
	}



	function Render($MODEL) {
		$subdomain = $this->subdomain;	// TODO: need to remove this but master.php looks for it because of the Branding thing

		// make sure user is logged in (unless the subclass allows anonymous access)
		$accountService = new AccountService();
		$user = $accountService->getCurrentUser();
		if (!$user && !$this->allowAnonymous) {
			header("Status: 200");
    		header("Location: /");
			exit;
		}

		// is this the googlebot? if so send an indexable, non angularjs version of the page
		if ($this->allowAnonymous && $this->optimizeForSearchBot) {
			$this->RenderForSearchBot();
			exit;
		}

		// TODO: the Render method is NOT the place for building hte model, move this somewhere else (BuildModel?)
		// user data
		$MODEL["user"] = $user;

		// aws data
		$MODEL['cfurl'] = 'http://'.CLOUDFRONT_URL;
		
		// codes data
		$codeService = new CodeService();
		$MODEL["mycodes"] = $user ? $codeService->recentactivity(null) : array();

		// reward data
		$rewardService = new RewardService();
		$MODEL["myrewards"] = $user ? $rewardService->myrewards(null) : array();
		$MODEL["userrewards"] = $user ? $rewardService->userrewards(null) : array();

		if ($user['businessId']) {
			// feedback data
			$feedbackService = new FeedbackService();
			$MODEL["feedback"] = $feedbackService->get();

			// account data
			$MODEL["paymentinfo"] = $accountService->get_payment_info();
			$MODEL["stripePublicKey"] = STRIPE_PUBLISHABLE_KEY;
		}

		include('../view/master.php');
	}

	function RenderForSearchBot() {
		$classname = get_class($this);
		$classNameParts = explode("Controller", $classname);
		$MODEL['viewName'] = strtolower($classNameParts[0]).'.html';
		$this->logger->write('Rendering '.$MODEL['viewName'].'.html for search bot');
		include('../view/seo.php');
	}
}

?>