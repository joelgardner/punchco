<?php

class RewardsController extends BaseController
{
	function __construct($request) {
		parent::__construct($request);
	}
	function Index() {
		$accountService = new AccountService();
		$user = $accountService->getCurrentUser();
		$businessId = $user["businessId"];

		$rewards = $this->db->get_rewards($businessId);
		$model = array(
			'rewards' => $rewards
		);
		$this->Render($model);
	}
}

?>