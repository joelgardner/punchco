<?php

class AccountController extends BaseController
{

	function Index() {
		//$accountService = new AccountService();
		//$user = $accountService->getCurrentUser();
		//$businessId = $user["businessId"];

		//$rewards = $this->db->get_rewards($businessId);
		$model = array(
			
		);
		$this->Render($model);
	}

	function Activate($signupKey) {
		// $id == signupKey
		$this->logger->write('hitting '.$signupKey);
		$accountService = new AccountService();
		$user = $accountService->getCurrentUser();

		// TODO: add some way to show a "you've activated your account" message

		$verifyResult = $accountService->verifySignupKey($signupKey);
		if ($verifyResult['Result'] == 0 && $user) {
			redirect_user("/codes");
		}
		else {
			redirect_user("/"); // if the account owner is not logged in, then they'll have to
		}
	}
}

?>