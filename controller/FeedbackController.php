<?php

class FeedbackController extends BaseController
{
	function Index() {
		$feedback = (new FeedbackService())->get();
		$model = array(
			"feedback" => $feedback
		);
		$this->Render($model);
	}

	function DisplayFeedback($code) {
		$logger = new Logger();
		$codeData = (new FeedbackService())->get_feedback_for_code($code);
		if (!$codeData) {
			redirect_user('/');
			exit;
		}
		unset($codeData['id']);
		unset($codeData['businessId']);
		
		$this->Render(array('code' => $codeData));
		

	}

	function Render($MODEL) {

		// aws data
		$MODEL['cfurl'] = 'http://'.CLOUDFRONT_URL;

		include('../view/feedback.php');
	}
}

?>