<?php

class ApiController extends BaseController
{
	function Execute($service, $action, $params) {
		if (!(new AccountService())->currentUserIsAllowedToPerformAction($service, $action)) {
			return;
		}
		if (!$this->validationService->validate($service, $action, $params)) {
			echo json_encode(array('Result' => 4, 'errorDesc' => "Invalid input"));
			return;
		}
		$serviceClassName = ucfirst($service)."Service";
		$serviceAction = array(new $serviceClassName, $action);
		echo json_encode(call_user_func_array($serviceAction, $params));
	}
}

?>