<?php

class PrintController extends BaseController
{

	function Index() {
		//$accountService = new AccountService();
		//$user = $accountService->getCurrentUser();
		//$businessId = $user["businessId"];

		//$rewards = $this->db->get_rewards($businessId);
		$model = array(
			//'rewards' => $rewards
		);
		$this->Render($model);
	}

	function batch($batchId) {
		$codeService = new CodeService();
		$model['codes'] = $codeService->get_codes_for_print(array(
			'batchId' => $batchId
		));
		$this->Render($model);
	}

	function Render($MODEL) {
		include('../view/print.php');
	}
}

?>