<?php

class DefaultController extends BaseController
{
	function Index($code = null) {
		$this->allowAnonymous = true;
		$model = array();
		if (isset($_GET['ggcode'])) {
			$model['punchCode'] = (new CodeService())->get_code($_GET['ggcode']);
		}
		$this->Render($model);
	}

	function BrandIndex($subdomain) {
		$account = array('name' => "Bill's Fajita Warehouse");
		

		$model = array(
			'account' => $account
		);
		$this->Render('branddefault', $model);
	}
}

?>